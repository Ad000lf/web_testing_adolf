import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// Login First 
WebUI.callTestCase(findTestCase('Web Testing/Login/TC_Login_Corp'), [:], FailureHandling.STOP_ON_FAILURE)

// Go to Monitoring Menu
WebUI.click(findTestObject('for_Menu/menu_Monitoring'))

// Click Button "Buat Tiket"
WebUI.click(findTestObject('for_Buat_Tiket/button_Buat Tiket'))

// Input the Resi
WebUI.setText(findTestObject('for_Buat_Tiket/textbox_Nomor Resi'), 'JX1977746209')

//Click Dropdown Aksi
WebUI.click(findTestObject('for_Buat_Tiket/dropdown_Aksi'))

// Select Return For The Aksi 
WebUI.click(findTestObject('for_Buat_Tiket/text_Return'))

// Set the info
WebUI.setText(findTestObject('for_Buat_Tiket/textbox_Info'), 'The Product Is Brokenn')

// Upload Lampiran
WebUI.uploadFile(findTestObject('for_Buat_Tiket/upload_Lampiran'), 'C:\\Users\\user\\Pictures\\DCI 1.PNG')

// Click Button Buat Tiket
WebUI.click(findTestObject('for_Buat_Tiket/button_Batal'))

