import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.chrome.ChromeDriver as ChromeDriver
import java.awt.Robot as Robot
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://sandbox-app.oexpress.co.id/login')

//Set the registerd email
WebUI.setText(findTestObject('for_Login/textbox_Email'), 'acolnaibaho007@gmail.com')

//Set the valid password
WebUI.setEncryptedText(findTestObject('for_Login/textbox_Password'), 'lN6pIkJRK07g2ziCikvpVA==')

//Click Button Masuk
WebUI.click(findTestObject('for_Login/button_Masuk'))

// Form Onboarding Register Member
WebUI.verifyElementText(findTestObject('for_Login/register_Member_page1_Corp/text_OnBoarding Registrasi Member'), 'OnBoarding Registrasi Member')

// Fill the data on page 1 
// Input your company name
WebUI.setText(findTestObject('for_Login/register_Member_page1_Corp/input_Nama Perusahaan'), 'XCORP')

// Input the owner name
WebUI.setText(findTestObject('for_Login/register_Member_page1_Corp/textbox_Nama Owner'), 'Zeuess')

// Input business fields
WebUI.setText(findTestObject('for_Login/register_Member_page1_Corp/textbox_Bidang Usaha_company'), 'Coal')

// Input your company NPWP
WebUI.setText(findTestObject('for_Login/register_Member_page1_Corp/textbox_NPWP'), '998790907655')

//  Input your company phone number
WebUI.setText(findTestObject('for_Login/register_Member_page1_Corp/textbox_No. Telepon_company'), '0627237865554')

// Input your company Email
WebUI.setText(findTestObject('for_Login/register_Member_page1_Corp/textbox_Email'), 'hc@corpx.co.id')

// Input your company address
WebUI.setText(findTestObject('for_Login/register_Member_page1_Corp/textbox_Alamat Lengkap'), 'Karet Gusuran IV')

// click dropdown Lokasi
WebUI.click(findTestObject('for_Login/register_Member_page1_Corp/textbox_Lokasi'))

// Input your Location
WebUI.setText(findTestObject('for_Login/register_Member_page1_Corp/textbox_Lokasi'), 'Setia budi')

// Select the option for your location
WebUI.click(findTestObject('for_Login/register_Member_page1_Corp/text_DKI Jakarta, Kota Jakarta Selatan, Setia Budi'))

// click button "selanjutnya" so we can move to second page
WebUI.click(findTestObject('for_Login/register_Member_page1_Corp/button_Selanjutnya'))

WebUI.delay(2)

// Set your department that make delivery 
WebUI.setText(findTestObject('for_Login/register_Member_page2_Corp/textbox_Nama Bagian Pengiriman'), 'WarehouseofCorp')

// Set your your company phone number 
WebUI.setText(findTestObject('for_Login/register_Member_page2_Corp/textbox_Nomor Telepon'), '06272378766')

// Set your Store Name 
WebUI.setText(findTestObject('for_Login/register_Member_page2_Corp/textbox_Nama Toko'), 'Baby X Corp')

// Set your finance name
WebUI.setText(findTestObject('for_Login/register_Member_page2_Corp/texboxt_Nama Finance'), 'Becca X Corp')

// Set work time 
// start Working
WebUI.setText(findTestObject('for_Login/register_Member_page2_Corp/textbox_Jam_Kerja_Mulai'), '0800')

// End Working
WebUI.setText(findTestObject('for_Login/register_Member_page2_Corp/textbox_Jam_Kerja_Akhir'), '1800')

// Set the day for working for this case select Senin - Sabtu
WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/text_Senin'))

WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/text_Selasa'))

WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/text_Rabu'))

WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/text_Kamis'))

WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/text_Jumat'))

WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/text_Sabtu'))

// Set your payment period 
WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/dropdown_Periode_Tagihan'))

WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/select_Bulanan'))

// Set your Warehouse location
WebUI.setText(findTestObject('for_Login/register_Member_page2_Corp/input_Alamat_warehouse'), 'Karet Gusuran XX')

WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/dropdown_Lokasi_Kota'))

WebUI.setText(findTestObject('for_Login/register_Member_page2_Corp/dropdown_Lokasi_Kota'), 'Cakung')

WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/seelect_DKI Jakarta, Kota Jakarta Timur, Cakung'))

// Set your payment method 
WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/text_Invoicing'))

WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/text_Harga Termasuk PPN'))

WebUI.click(findTestObject('for_Login/register_Member_page2_Corp/text_SATSET'))

// Click Selanjutnya button so we can move to Third page
WebUI.click(findTestObject('for_Login/register_Member_page1_Corp/button_Selanjutnya'))

WebUI.delay(2)

//Upload KTP
WebUI.click(findTestObject('for_Login/register_Member_page3_Corp/upload_KTP'))

String filePath_1 = 'C:\\Users\\user\\Pictures\\a.PNG'

Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(filePath_1), null)

Robot robot_1 = new Robot()

robot_1.keyPress(KeyEvent.VK_CONTROL)

robot_1.keyPress(KeyEvent.VK_V)

robot_1.keyRelease(KeyEvent.VK_V)

robot_1.keyRelease(KeyEvent.VK_CONTROL)

robot_1.keyRelease(KeyEvent.VK_CONTROL)

robot_1.keyPress(KeyEvent.VK_ENTER)

robot_1.keyRelease(KeyEvent.VK_ENTER)

// Upload NPWP
WebUI.click(findTestObject('for_Login/register_Member_page3_Corp/upload_NPWP'))

String filePath_2 = 'C:\\Users\\user\\Pictures\\b.PNG'

Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(filePath_2), null)

Robot robot_2 = new Robot()

robot_2.keyPress(KeyEvent.VK_CONTROL)

robot_2.keyPress(KeyEvent.VK_V)

robot_2.keyRelease(KeyEvent.VK_V)

robot_2.keyRelease(KeyEvent.VK_CONTROL)

robot_2.keyRelease(KeyEvent.VK_CONTROL)

robot_2.keyPress(KeyEvent.VK_ENTER)

robot_2.keyRelease(KeyEvent.VK_ENTER)

// Uploadn SIUP
WebUI.click(findTestObject('for_Login/register_Member_page3_Corp/upload_SIUP'))

String filePath_3 = 'C:\\Users\\user\\Pictures\\c.PNG'

Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(filePath_3), null)

Robot robot_3 = new Robot()

robot_3.keyPress(KeyEvent.VK_CONTROL)

robot_3.keyPress(KeyEvent.VK_V)

robot_3.keyRelease(KeyEvent.VK_V)

robot_3.keyRelease(KeyEvent.VK_CONTROL)

robot_3.keyRelease(KeyEvent.VK_CONTROL)

robot_3.keyPress(KeyEvent.VK_ENTER)

robot_3.keyRelease(KeyEvent.VK_ENTER)

// Click Selanjutnya button so we can move to Fourh page
WebUI.click(findTestObject('for_Login/register_Member_page1_Corp/button_Selanjutnya'))

WebUI.delay(2)

// Fill the data on page Fourth
// Select the bank name
WebUI.click(findTestObject('for_Login/for_Store_Information_Page3/div_Pilih Bank'))

WebUI.click(findTestObject('for_Login/for_Store_Information_Page3/select_BCA'))

// Set Account number
WebUI.setText(findTestObject('for_Login/for_Store_Information_Page3/textbox_Nomor_Rekening'), 123456789999)

// Set Account Name
WebUI.setText(findTestObject('for_Login/for_Store_Information_Page3/textbox_Nama_Pemilik_Rekening'), Sutyono)

WebUI.click(findTestObject('for_Login/register_Member_page1_Corp/button_Selanjutnya'))

WebUI.delay(2)

// Pop up Messages Appears
WebUI.verifyElementText(findTestObject('for_Login/for_Form_Berhasil/text_Berhasil'), 'Berhasil')

WebUI.verifyElementText(findTestObject('for_Login/for_Form_Berhasil/text_Data anda yang diisi telah disimpan'), 'Data anda yang diisi telah disimpan.')

WebUI.click(findTestObject('for_Login/for_Form_Berhasil/button_Ya, Mengerti'))


// On the screen will got User name and Job
// Verify the user name
WebUI.verifyElementVisible(findTestObject('for_Menu/text_Nama_User'))

// Verify username job
WebUI.verifyElementText(findTestObject('for_Menu/text_Social Commerce'), 'Corporate')


