import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// Login First
WebUI.callTestCase(findTestCase('Web Testing/Login/TC_Login_Corp'), [:], FailureHandling.STOP_ON_FAILURE)

// Click Riwayat Kiriman Menu 
WebUI.click(findTestObject('for_Menu/menu_Riwayat Kiriman'))

// Click Submenu Pickup
WebUI.click(findTestObject('for_Pickup/submenu_Pickup'))

WebUI.delay(5)

// Click Date Dropdown 
WebUI.click(findTestObject('for_Pickup/set_Date'))

// Click 90 Hari lalu 
WebUI.click(findTestObject('for_Pickup/text_90 hari lalu'))

WebUI.delay(5)

WebUI.click(findTestObject('for_Pickup/dropdown_Pilih_Kurir'))

// Choose the Kurir 
WebUI.click(findTestObject('for_Pickup/set_JNE Express'))

// Verify the Kurir Had selected 
WebUI.verifyElementText(findTestObject('for_Pickup/text_Selected_Kurir'), 'JNE Express')

WebUI.delay(5)

// Set The Warehouse
WebUI.click(findTestObject('for_Pickup/dropdown_Pilih_WareHouse'))

WebUI.click(findTestObject('for_Pickup/span_Acol'))

WebUI.delay(5)

// Verify the result 
WebUI.verifyElementVisible(findTestObject('for_Pickup/icon_Databases'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('for_Pickup/text_Tidak Ada Kiriman'), 'Tidak Ada Kiriman')

WebUI.verifyElementClickable(findTestObject('for_Pickup/button_Buat Kiriman'))

