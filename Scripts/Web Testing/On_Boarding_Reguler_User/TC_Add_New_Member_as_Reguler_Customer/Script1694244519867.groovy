import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Web Testing/Login/TC_Login_Reguler_User'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('for_Menu/menu_Pengaturan'))

WebUI.click(findTestObject('for_Pengaturan/submenu_Anggota Tim'))

WebUI.click(findTestObject('for_Pengaturan/for_Anggota_Team/button_Tambah Member Baru'))

WebUI.setText(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/textbox_Nama(Tambah_Member_Baru)'), 
    'Shynie')


// Make Random Email to make test easy
// Define the characters that can be used in the random string
String characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'

// Define the length of the random string you want to generate
int length = 10 // Change this to the desired length

// Create a StringBuilder to build the random string
StringBuilder randomString = new StringBuilder()

// Create an instance of the Random class
Random random = new Random()

// Generate a random string of the specified length
for (int i = 0; i < length; i++) {
	int index = random.nextInt(characters.length())

	char randomChar = characters.charAt(index)

	randomString.append(randomChar)
}

// Use a different variable name for characters used in the random email generation
String emailCharacters = 'abcdefghijklmnopqrstuvwxyz0123456789'

// Define the length of the random username
int usernameLength = 8 // Change this to the desired length

// Create a StringBuilder to build the random username
StringBuilder randomUsername = new StringBuilder()

// Generate random username of the specified length
for (int i = 0; i < usernameLength; i++) {
	int index = random.nextInt(emailCharacters.length())

	char randomChar = emailCharacters.charAt(index)

	randomUsername.append(randomChar)
}

// Concatenate the random username with "@gmail.com" to create the email address
String randomEmail = randomUsername.toString() + '@gmail.com'

println("Random Email Address: $randomEmail")


WebUI.setText(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/textbox_Email(Tambah_Member_Baru)'), 
    randomEmail)

WebUI.setEncryptedText(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/textboxt_Kata Sandi(Tambah_Member_Baru)'), 
    'lN6pIkJRK07g2ziCikvpVA==')

WebUI.setText(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/textbox_Nomor Telepon(Tambah_Member_Baru)'), 
    '089799771177')

WebUI.click(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/dropdown_Role'))

WebUI.click(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/select_Customer Service'))

WebUI.check(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/checkbox_Kiriman'))

WebUI.check(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/checkbox_Saldo'))

WebUI.click(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/button_Simpan'))

WebUI.verifyElementText(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/text_Berhasil Menambahkan'), 
    'Berhasil Menambahkan!')

WebUI.verifyElementText(findTestObject('for_Pengaturan/for_Anggota_Team/for_Tambah Member Baru/text_Member berhasil ditambahkan'), 
    'Member berhasil ditambahkan.')


