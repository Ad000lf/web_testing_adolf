import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// Login First
WebUI.callTestCase(findTestCase('Web Testing/Login/TC_Login_Reguler_User'), [:], FailureHandling.STOP_ON_FAILURE)

// Click "Pengaturan" Menu 
WebUI.click(findTestObject('for_Menu/menu_Pengaturan'))

// Verify after click Pengaturan system will show some sub menu 
WebUI.verifyElementText(findTestObject('for_Pengaturan/submenu_Pengaturan Akun'), 'Pengaturan Akun')

WebUI.verifyElementText(findTestObject('for_Pengaturan/submenu_Anggota Tim'), 'Anggota Tim')

WebUI.verifyElementText(findTestObject('for_Pengaturan/submenu_Template Kiriman'), 'Template Kiriman')

WebUI.verifyElementText(findTestObject('for_Pengaturan/submenu_Akun Bank'), 'Akun Bank')

WebUI.verifyElementText(findTestObject('for_Pengaturan/submenu_Pengaturan Toko'), 'Pengaturan Toko')

WebUI.verifyElementText(findTestObject('for_Pengaturan/submenu_Follow up'), 'Follow up')

WebUI.verifyElementText(findTestObject('for_Pengaturan/submenu_Pengaturan Kurir'), 'Pengaturan Kurir')

WebUI.verifyElementText(findTestObject('for_Pengaturan/submenu_Alamat Saya'), 'Alamat Saya')

WebUI.verifyElementText(findTestObject('for_Pengaturan/submenu_Third Party'), 'Third Party')

// Click submenu "Pengaturan Akun"
WebUI.click(findTestObject('for_Pengaturan/submenu_Pengaturan Akun'))

// Clear the text on the Nama Textbox
WebUI.clearText(findTestObject('for_Pengaturan/for_Pengaturan_Akun/textbox_Nama'))

// Input new name 
String new_Name = 'Col007'

WebUI.setText(findTestObject('for_Pengaturan/for_Pengaturan_Akun/textbox_Nama'), new_Name)

// Click Button "Simpan Perubahan"
WebUI.click(findTestObject('for_Pengaturan/for_Pengaturan_Akun/button_Simpan Perubahan'))

// Verify notification message will pop up
WebUI.verifyElementText(findTestObject('for_Pengaturan/for_Pengaturan_Akun/text_Success'), 'Success')

WebUI.verifyElementText(findTestObject('for_Pengaturan/for_Pengaturan_Akun/text_Your profile was changed'), 'Your profile was changed')

// Verify your new name same with the name on the right top 
WebUI.verifyElementText(findTestObject('for_Menu/text_Nama_User'), new_Name)

