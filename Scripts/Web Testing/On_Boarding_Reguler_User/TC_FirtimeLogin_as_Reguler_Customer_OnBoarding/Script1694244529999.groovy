import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.chrome.ChromeDriver as ChromeDriver
import java.awt.Robot as Robot
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.StringSelection as StringSelection
import java.awt.event.KeyEvent as KeyEvent

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://sandbox-app.oexpress.co.id/login')

//Set the registerd email
WebUI.setText(findTestObject('for_Login/textbox_Email'), 'nindy.panjaitan@pgi-data.com')

//Set the valid password
WebUI.setEncryptedText(findTestObject('for_Login/textbox_Password'), 'lN6pIkJRK07g2ziCikvpVA==')

//Click Button Masuk
WebUI.click(findTestObject('for_Login/button_Masuk'))

// Verify And fill the data on page one
WebUI.verifyElementVisible(findTestObject('for_Login/for_Store_Information_Page1/text_Yuk ikuti langkah dibawah ini untuk melengkapi data toko kamu'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementText(findTestObject('for_Menu/selected_DashBoard'), 'Yuk ikuti langkah dibawah ini untuk melengkapi data toko kamu')

WebUI.setText(findTestObject('for_Login/for_Store_Information_Page1/textbox_Nama Toko_store_name'), 'Sejahtera ')

WebUI.setText(findTestObject('for_Login/for_Store_Information_Page1/textbox_Nama Owner_name'), 'Ninday')

WebUI.setText(findTestObject('for_Login/for_Store_Information_Page1/textbox_Nomor Telepon'), '081378968979')

WebUI.setText(findTestObject('for_Login/for_Store_Information_Page1/textbox_Alamat_pickup'), 'Jln Kemanggisan no 23')

WebUI.setText(findTestObject('for_Login/for_Store_Information_Page1/textbox_Lokasi Kota Kecamatan'), 'DKI Jakarta, Kota Jakarta Selatan, Setia Budi')

WebUI.click(findTestObject('for_Login/for_Store_Information_Page1/text_DKI Jakarta, Kota Jakarta Selatan, Setia Budi'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('for_Login/for_Store_Information_Page1/button_Selanjutnya'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

// Verify dan set data for page 2
// Click radio button Gunakan Alamat PerusahaanToko Sebagai Alamat Pickup 
WebUI.click(findTestObject('for_Login/for_Store_Informatio_Page2/text_Gunakan Alamat PerusahaanToko Sebagai Alamat Pickup'), 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('for_Login/for_Store_Information_Page1/button_Selanjutnya'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

// Verify dan set data for page 3
WebUI.click(findTestObject('for_Login/for_Store_Information_Page3/div_Pilih Bank'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('for_Login/for_Store_Information_Page3/select_BCA'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('for_Login/for_Store_Information_Page3/textbox_Nomor_Rekening'), '111123334555')

WebUI.click(findTestObject('for_Login/for_Store_Information_Page3/textbox_Nomor_Rekening'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('for_Login/for_Store_Information_Page3/div_Bank_multiselect__select'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('for_Login/for_Store_Information_Page1/button_Selanjutnya'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

// Verify dan set data for page 4
// Upload KTP
WebUI.click(findTestObject('for_Login/for_Store_Information_Page4/panel_Upload_KTP'))

String filePath = 'C:\\Users\\user\\Pictures\\a.PNG'

Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(filePath), null)

Robot robot = new Robot()

robot.keyPress(KeyEvent.VK_CONTROL)

robot.keyPress(KeyEvent.VK_V)

robot.keyRelease(KeyEvent.VK_V)

robot.keyRelease(KeyEvent.VK_CONTROL)

robot.keyRelease(KeyEvent.VK_CONTROL)

robot.keyPress(KeyEvent.VK_ENTER)

robot.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(5)

WebUI.click(findTestObject('for_Login/for_Store_Information_Page1/button_Selanjutnya'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

// Pop up Messages Appears
WebUI.verifyElementText(findTestObject('for_Login/for_Form_Berhasil/text_Berhasil'), 'Berhasil')

WebUI.verifyElementText(findTestObject('for_Login/for_Form_Berhasil/text_Data anda yang diisi telah disimpan'), 'Data anda yang diisi telah disimpan.')

WebUI.click(findTestObject('for_Login/for_Form_Berhasil/button_Ya, Mengerti'))

// On the screen will got User name and Job
// Verify the user name
WebUI.verifyElementVisible(findTestObject('for_Menu/text_Nama_User'))

// Verify username job
WebUI.verifyElementText(findTestObject('for_Menu/text_Social Commerce'), 'Social Commerce')



