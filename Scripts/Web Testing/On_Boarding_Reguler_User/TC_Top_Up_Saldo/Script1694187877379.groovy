import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// Login First
WebUI.callTestCase(findTestCase('Web Testing/Login/TC_Login_Reguler_User'), [:], FailureHandling.STOP_ON_FAILURE)

// Click Menu "Saldo"
WebUI.click(findTestObject('for_Menu/menu_Saldo'))

// Click button "Top Up Saldo"
WebUI.click(findTestObject('for_Saldo/for_Topup_Saldo/button_Topup Saldo'))

// Verify form To Up Saldo Will Apear with text Top Up Saldo
WebUI.verifyElementVisible(findTestObject('for_Saldo/for_Topup_Saldo/form_Top_Up_Saldo'))

WebUI.verifyElementText(findTestObject('for_Saldo/for_Topup_Saldo/text_Top Up Saldo'), 'Top Up Saldo')

WebUI.clearText(findTestObject('for_Saldo/for_Topup_Saldo/textbox_Masukkan Saldo'), FailureHandling.STOP_ON_FAILURE)

// Set the amount than you want to top on "Masukkan Saldo" textbox 
WebUI.setText(findTestObject('for_Saldo/for_Topup_Saldo/textbox_Masukkan Saldo'), '10000000')

// Click Top Up Button 
WebUI.click(findTestObject('for_Saldo/for_Topup_Saldo/button_Top Up'))

