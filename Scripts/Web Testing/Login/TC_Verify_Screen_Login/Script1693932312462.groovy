import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://sandbox-app.oexpress.co.id/login')

// Verify Login Page 
// On Login Page there was a Logo OXPRESS
WebUI.verifyElementVisible(findTestObject('for_Login/img_OEXPRESS'))

// Verify text email text on Login Page
WebUI.verifyElementText(findTestObject('for_Login/text_Email'), 'Email')

// Verify Email textbox on Login Page
WebUI.verifyElementVisible(findTestObject('for_Login/textbox_Email'))

// Verify text password text on Login Page
WebUI.verifyElementText(findTestObject('for_Login/text_Password'), 'Password')

// Verify Password textbox on Login Page
WebUI.verifyElementVisible(findTestObject('for_Login/textbox_Password'))

// Verify Masuk Button on Login Page
WebUI.verifyElementVisible(findTestObject('for_Login/button_Masuk'))

// Verify text 'Belum memiliki akun?'
WebUI.verifyElementVisible(findTestObject('for_Login/text_Belum memiliki akun Daftar Disini'), FailureHandling.STOP_ON_FAILURE)

// Verify link 'Daftar Disini' 
WebUI.verifyElementText(findTestObject('for_Login/link_Daftar Disini'), 'Daftar Disini')

