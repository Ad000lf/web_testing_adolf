import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Web Testing/Login/TC_Login_Corp'), [:], FailureHandling.STOP_ON_FAILURE)

//Click The User Name on the Right Top 
WebUI.click(findTestObject('for_Menu/text_Nama_User'))

//Then Click Logout
WebUI.click(findTestObject('for_LogOut/text_Logout'))

//Verify user that sucessfull to logout 
// Verify OEXPRESS LOGO 
WebUI.verifyElementVisible(findTestObject('for_LogOut/img_OEXPRESS'))

// Verify text "Sampai Jumpa Kembali" 
WebUI.verifyElementText(findTestObject('for_LogOut/text_Sampai Jumpa Kembali'), 'Sampai Jumpa Kembali!')

//Verify text "Anda telah berhasil keluar dari oexpress.id" 
WebUI.verifyElementText(findTestObject('for_LogOut/text_Anda telah berhasil keluar dari oexpress.id'), 'Anda telah berhasil keluar dari oexpress.id')

//Verify Checklist Logo Appear 
WebUI.verifyElementVisible(findTestObject('for_LogOut/img_Checklist'))

//Verify text Login 
WebUI.verifyElementText(findTestObject('for_LogOut/link_Log In'), 'Log In')

WebUI.closeBrowser()

