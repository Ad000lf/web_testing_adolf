import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://sandbox-app.oexpress.co.id/login')

//Set the registerd email
WebUI.setText(findTestObject('for_Login/textbox_Email'), 'acoldong@gmail.com')

//Set the valid password 
WebUI.setEncryptedText(findTestObject('for_Login/textbox_Password'), 'lN6pIkJRK06J869Zof78bw==')

//Click Button Masuk 
WebUI.click(findTestObject('for_Login/button_Masuk'))

//Dashboard is selected
WebUI.verifyElementVisible(findTestObject('for_Menu/selected_DashBoard'))

// On the screen will got User name and Job 
// Verify the user name 
WebUI.verifyElementVisible(findTestObject('for_Menu/text_Nama_User'))

// Verify username job 
WebUI.verifyElementText(findTestObject('for_Menu/text_Social Commerce'), 'Social Commerce')

// On the screen will provide some menu
// Verify Dashboard Menu 
WebUI.verifyElementVisible(findTestObject('for_Menu/menu_Dashboard'))

/// Verify Riwayat Kiriman Menu 
WebUI.verifyElementVisible(findTestObject('for_Menu/menu_Riwayat Kiriman'))

// Verify Menu Saldo
WebUI.verifyElementVisible(findTestObject('for_Menu/menu_Saldo'))

// Verify Menu Monitoring
WebUI.verifyElementVisible(findTestObject('for_Menu/menu_Monitoring'))

// Verify Menu Tagihan
WebUI.verifyElementVisible(findTestObject('for_Menu/menu_Tagihan'))

// Verify Menu Pengaturan
WebUI.verifyElementVisible(findTestObject('for_Menu/menu_Pengaturan'))

