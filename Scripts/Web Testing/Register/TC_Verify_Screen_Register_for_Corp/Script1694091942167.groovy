import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://sandbox-app.oexpress.co.id/corp/register')

// Verify Register Page
// On Register Page there was a Logo OXPRESS
WebUI.verifyElementVisible(findTestObject('for_Login/img_OEXPRESS'))

// Verify text "Nama Lengkap"
WebUI.verifyElementText(findTestObject('for_Register/text_Nama Lengkap'), 'Nama Perusahaan')

// Verify textbox "Nama Lengkap" 
WebUI.verifyElementVisible(findTestObject('for_Register/textbox_Nama_Lengkap'))

// Verify text "Email" 
WebUI.verifyElementText(findTestObject('for_Register/text_Email'), 'Email')

// Verify textbox "Email"
WebUI.verifyElementVisible(findTestObject('for_Register/textbox_Email'))

// Verify text "Password"
WebUI.verifyElementText(findTestObject('for_Register/text_Password'), 'Password')

// Verify textbox "Password"
WebUI.verifyElementVisible(findTestObject('for_Register/textbox_Password'))

// Verify text "Minimal harus 8 karakter"
WebUI.verifyElementText(findTestObject('for_Register/text_Minimal harus 8 karakter'), 'Minimal harus 8 karakter')

// Verify text "Maximal harus 64 karakter"
WebUI.verifyElementText(findTestObject('for_Register/text_Maximal harus 64 karakter'), 'Maximal harus 64 karakter')

// Verify text "Disarankan menggunakan kombinasi huruf kapital, huruf kecil, angka dan karakter khusus"
WebUI.verifyElementText(findTestObject('for_Register/text_Disarankan menggunakan kombinasi huruf kapital, huruf kecil, angka dan karakter khusus'), 
    'Disarankan menggunakan kombinasi huruf kapital, huruf kecil, angka dan karakter khusus')

// Verify text "Nomor Handphone"
WebUI.verifyElementText(findTestObject('for_Register/text_Nomor Handphone'), 'Nomor Handphone')

// Verify Button "Daftar"
WebUI.verifyElementVisible(findTestObject('for_Register/button_Daftar'))

WebUI.closeBrowser()

