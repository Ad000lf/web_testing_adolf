import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://sandbox-app.oexpress.co.id/register')

// Set the User Name in "Nama Lengkap" textbox
WebUI.setText(findTestObject('for_Register/textbox_Nama_Lengkap'), 'John')

// Set the password in "Password" textbox
WebUI.setEncryptedText(findTestObject('for_Register/textbox_Password'), 'lN6pIkJRK07g2ziCikvpVA==')

WebUI.setText(findTestObject('for_Register/textbox_Email'), 'aa')

WebUI.clearText(findTestObject('for_Register/textbox_Email'), FailureHandling.STOP_ON_FAILURE)

// Set the Phone number in "Nomor Handphone" textbox
WebUI.setText(findTestObject('for_Register/textbox_Nomor Handphone'), '0813961238967')

// Click "Daftar" Button
WebUI.click(findTestObject('for_Register/button_Daftar'))

// Verify mesaage "Email sudah terdaftar di dalam sistem"
WebUI.verifyElementText(findTestObject('for_Register/for_Invalid_Register/message_email tidak boleh kosong'), 'email tidak boleh kosong.')

