<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_Log In</name>
   <tag></tag>
   <elementGuidId>6e5f7457-6ada-4381-a5a4-536685dc3b61</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a/b</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.text-danger.ms-1 > b</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>b</value>
      <webElementGuid>40915b7d-039c-4413-98cc-476e6c1c43e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Log In</value>
      <webElementGuid>53673fab-1c60-49c5-81f8-db988959bb59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;layout-auth&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;email-verification&quot;]/footer[@class=&quot;footer-alt mt-3&quot;]/p[@class=&quot;text-muted&quot;]/a[@class=&quot;text-danger ms-1&quot;]/b[1]</value>
      <webElementGuid>6281e6cd-9e98-4048-8a3c-0eed1ee15b72</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/footer/p/a/b</value>
      <webElementGuid>93bf551e-adea-4f2e-a85b-697528c606b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='IndahH@gmail.com'])[1]/following::b[1]</value>
      <webElementGuid>c5e0f086-3ef9-4c76-9cd8-aa9d3a53040c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please Check Your Email!'])[1]/following::b[2]</value>
      <webElementGuid>2301319d-c600-45ad-b92e-48842070f786</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::b[1]</value>
      <webElementGuid>10932136-bbfc-4158-b02b-1217dfa0802c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::b[1]</value>
      <webElementGuid>f6bf2cb0-7319-4c2f-928a-61d12d203083</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Log In']/parent::*</value>
      <webElementGuid>244a15da-7307-4293-bc17-ff4eda962752</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/b</value>
      <webElementGuid>a097eb70-c92e-4d99-8360-0b74e64c002e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//b[(text() = 'Log In' or . = 'Log In')]</value>
      <webElementGuid>04c1d78a-d9e7-461e-97e8-a857538df0d6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
