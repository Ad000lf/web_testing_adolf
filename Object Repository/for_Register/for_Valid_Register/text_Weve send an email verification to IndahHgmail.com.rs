<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Weve send an email verification to IndahHgmail.com</name>
   <tag></tag>
   <elementGuidId>f655c704-cdad-46ee-b5ed-722129406dfd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[1]/div/div/div[2]/div[1]/p/text()</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.text-muted.mb-4</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//p[(text() = concat(&quot;We&quot; , &quot;'&quot; , &quot;ve send an email verification to IndahH@gmail.com&quot;) or . = concat(&quot;We&quot; , &quot;'&quot; , &quot;ve send an email verification to IndahH@gmail.com&quot;))]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>654e0990-8fa5-4c80-8960-b9fab5106e4c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-muted mb-4</value>
      <webElementGuid>d4033593-89b7-4bcc-a104-e3f4c0de8a6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>We've send an email verification to IndahH@gmail.com</value>
      <webElementGuid>598ee1f4-b7c2-48e2-975b-2723b0ca22a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;layout-auth&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;email-verification&quot;]/div[@class=&quot;text-center&quot;]/p[@class=&quot;text-muted mb-4&quot;]</value>
      <webElementGuid>f1b78d0e-dd4b-4190-942e-4d04d29770f3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/p</value>
      <webElementGuid>ffdc394d-39e4-44a5-8f33-e2a5d543f71c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Please Check Your Email!'])[1]/following::p[1]</value>
      <webElementGuid>f846f532-784f-458f-b825-eeedbe86e4a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log In'])[1]/preceding::p[1]</value>
      <webElementGuid>aa05ce61-0d99-4277-b80c-a16dbb8a7fe8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>80a94224-6a47-4329-95f9-cf4ac18db26c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = concat(&quot;We&quot; , &quot;'&quot; , &quot;ve send an email verification to IndahH@gmail.com&quot;) or . = concat(&quot;We&quot; , &quot;'&quot; , &quot;ve send an email verification to IndahH@gmail.com&quot;))]</value>
      <webElementGuid>4555b16a-49d9-46eb-b6b0-76d1fabc67cd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
