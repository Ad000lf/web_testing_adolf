<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Maximal harus 64 karakter</name>
   <tag></tag>
   <elementGuidId>177bc009-6ddd-4c08-99bc-e6155cade831</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/form/div[3]/div[2]/div[2]/div[2]/span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>db8ba084-ab2b-4e6b-a7ca-0048124d1ee2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>validation-text undefined</value>
      <webElementGuid>eb50e1c7-8f70-426a-9a8d-ea3fd7cf85b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Maximal harus 64 karakter</value>
      <webElementGuid>a2e97611-a326-4b92-a7ac-ac5f89772ce8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;layout-auth&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[2]/form[1]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;d-flex flex-column mt-2 gap-2&quot;]/div[2]/div[@class=&quot;d-flex gap-1&quot;]/span[@class=&quot;validation-text undefined&quot;]</value>
      <webElementGuid>29b6f03f-9d30-47f6-a701-aace8bbc1ef3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/form/div[3]/div[2]/div[2]/div[2]/span[2]</value>
      <webElementGuid>18e76efa-c9d6-484e-9850-1bc684efec05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Minimal harus 8 karakter'])[1]/following::span[2]</value>
      <webElementGuid>a559511b-aed3-4b79-816a-701c4f3623c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::span[5]</value>
      <webElementGuid>422043c4-935e-4866-ae1d-f06e7162cde6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Disarankan menggunakan kombinasi huruf kapital, huruf kecil, angka dan karakter khusus'])[1]/preceding::span[2]</value>
      <webElementGuid>5ff86831-835d-456e-9875-9c57c672ac36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nomor Handphone'])[1]/preceding::span[3]</value>
      <webElementGuid>70bfb27e-6f55-40e6-b367-7fad76f11f57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Maximal harus 64 karakter']/parent::*</value>
      <webElementGuid>c65368f4-0f60-43ff-95e7-032a9e1ecfdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/span[2]</value>
      <webElementGuid>e2ffa20c-9a89-49f6-9ed2-932858561511</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Maximal harus 64 karakter' or . = 'Maximal harus 64 karakter')]</value>
      <webElementGuid>27381cf7-2f14-4f3b-b400-e18e0da099f1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
