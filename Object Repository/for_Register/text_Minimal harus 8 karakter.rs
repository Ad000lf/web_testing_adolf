<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Minimal harus 8 karakter</name>
   <tag></tag>
   <elementGuidId>b720cbc1-1470-401a-96d2-c87c9dad3491</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/form/div[3]/div[2]/div[2]/div/span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.d-flex.flex-column.mt-2.gap-2 > div:nth-of-type(2) > div.d-flex.gap-1 > span.validation-text.undefined</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>9b2aaf25-3ab7-4918-8fb8-e9c987864b9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>validation-text undefined</value>
      <webElementGuid>4d3f1e3b-7333-4439-8dce-f8b988744594</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Minimal harus 8 karakter</value>
      <webElementGuid>0abd7561-7321-42aa-95bd-6640e4e3e082</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;layout-auth&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[2]/form[1]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;d-flex flex-column mt-2 gap-2&quot;]/div[2]/div[@class=&quot;d-flex gap-1&quot;]/span[@class=&quot;validation-text undefined&quot;]</value>
      <webElementGuid>24e052cd-415d-4d57-b0b6-b5ff5e0d9f0d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/form/div[3]/div[2]/div[2]/div/span[2]</value>
      <webElementGuid>f3ada942-4d92-4bb6-b88e-20cf96dd4bb6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::span[3]</value>
      <webElementGuid>58e79519-582d-4212-83c1-c4f0d8ec6014</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::span[3]</value>
      <webElementGuid>12b3ec0e-7c0b-4cf3-b197-9309e904446b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Maximal harus 64 karakter'])[1]/preceding::span[2]</value>
      <webElementGuid>a1d0c2bd-9d9d-4e0b-8b05-f13bb0d1f1ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Disarankan menggunakan kombinasi huruf kapital, huruf kecil, angka dan karakter khusus'])[1]/preceding::span[4]</value>
      <webElementGuid>3ab977a1-5035-47bf-8689-7afb3ee61c6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Minimal harus 8 karakter']/parent::*</value>
      <webElementGuid>1e49776c-957d-4c57-8d8f-3bed53f9d6bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>1120969c-95e1-4bce-92d5-5e5a91e1203c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Minimal harus 8 karakter' or . = 'Minimal harus 8 karakter')]</value>
      <webElementGuid>830d7ec9-81de-4fb1-8973-0c5f75b04ebf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
