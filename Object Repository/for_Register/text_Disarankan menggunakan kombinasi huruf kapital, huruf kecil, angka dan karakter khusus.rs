<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Disarankan menggunakan kombinasi huruf kapital, huruf kecil, angka dan karakter khusus</name>
   <tag></tag>
   <elementGuidId>452b32f0-7bab-45ed-b3e9-fbf0dff93df6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/form/div[3]/div[2]/div[2]/div[4]/span[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>400611f0-bff4-4e96-895b-948aa6849692</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>validation-text undefined</value>
      <webElementGuid>8fce19e9-3f6e-42c1-bbe6-5e7ab416fa54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Disarankan menggunakan kombinasi huruf kapital, huruf kecil, angka dan karakter khusus</value>
      <webElementGuid>f599b324-a063-4ec0-bd86-403e7e845835</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;layout-auth&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[2]/form[1]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;d-flex flex-column mt-2 gap-2&quot;]/div[2]/div[@class=&quot;d-flex gap-1&quot;]/span[@class=&quot;validation-text undefined&quot;]</value>
      <webElementGuid>0dc8a1ed-99f6-4afe-9f89-5a8d22d938cb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/form/div[3]/div[2]/div[2]/div[4]/span[2]</value>
      <webElementGuid>cf64255b-bb10-446c-9be1-8b1d9302b373</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Maximal harus 64 karakter'])[1]/following::span[2]</value>
      <webElementGuid>9b24c83b-2c16-4ca3-9248-6bd8cf81aacd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Minimal harus 8 karakter'])[1]/following::span[4]</value>
      <webElementGuid>d99499df-8713-4806-8f74-0cee304d4e98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nomor Handphone'])[1]/preceding::span[1]</value>
      <webElementGuid>fc694902-0e4f-489d-9c2c-8629998a0824</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar'])[1]/preceding::span[1]</value>
      <webElementGuid>59b855ea-93b5-42e5-b744-6cc670b4a57e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Disarankan menggunakan kombinasi huruf kapital, huruf kecil, angka dan karakter khusus']/parent::*</value>
      <webElementGuid>6b708966-e5df-404f-ac3e-c152ac7dafc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/span[2]</value>
      <webElementGuid>378b656b-72cc-47c8-8ce9-131904656d61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Disarankan menggunakan kombinasi huruf kapital, huruf kecil, angka dan karakter khusus' or . = 'Disarankan menggunakan kombinasi huruf kapital, huruf kecil, angka dan karakter khusus')]</value>
      <webElementGuid>4c8daa82-e943-4e76-86da-aee34b9074f2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
