<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Status_On Hold</name>
   <tag></tag>
   <elementGuidId>f44ce5af-db0b-4f7a-8cec-fdd16920b5a2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='null-5']/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>c2ad618c-6787-43ce-9d38-a5ab3e159432</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>On Hold</value>
      <webElementGuid>aec9ca8f-bcce-4f59-ba87-8c5408cc919b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;filter-order d-flex justify-content-between&quot;]/div[@class=&quot;filter-order-form align-items-center&quot;]/div[@class=&quot;filter-group grid-tc-4&quot;]/div[2]/div[@class=&quot;multiselect--active multiselect--above multiselect select-status&quot;]/div[@class=&quot;multiselect__content-wrapper&quot;]/ul[@id=&quot;listbox-null&quot;]/li[@id=&quot;null-5&quot;]/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>c2060894-5347-484f-8f3c-296612c4fe48</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-5']/span/span</value>
      <webElementGuid>8707bd85-7614-4526-ae46-4d60d0c9aa01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/following::span[2]</value>
      <webElementGuid>6d4910e6-d9ce-4849-bcce-0dec21bfdb9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delivered'])[1]/following::span[4]</value>
      <webElementGuid>6f117e3a-5769-4f57-9fdb-c654335b4846</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reject'])[1]/preceding::span[1]</value>
      <webElementGuid>2cb4e436-e67a-4b70-a7d3-47e65e04a55a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lost'])[1]/preceding::span[3]</value>
      <webElementGuid>ca601e66-0b56-49a6-bdae-02dfaae30490</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='On Hold']/parent::*</value>
      <webElementGuid>6aea746c-57ce-4d8d-96c1-6905ced9eda7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/span/span</value>
      <webElementGuid>7918acd9-6bf6-4cc9-9bff-a490f746fb48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'On Hold' or . = 'On Hold')]</value>
      <webElementGuid>9e78bcba-b826-4332-ab67-53931ce7ddcf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
