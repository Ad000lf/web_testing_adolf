<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Status_Return</name>
   <tag></tag>
   <elementGuidId>1c760bf4-34bd-48f5-aff5-155108ef0ce9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='null-4']/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>431c6386-264a-4370-b18a-d2513977b41c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Return</value>
      <webElementGuid>6c9799f2-90ff-4bd9-a46c-9b395c47a524</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;filter-order d-flex justify-content-between&quot;]/div[@class=&quot;filter-order-form align-items-center&quot;]/div[@class=&quot;filter-group grid-tc-4&quot;]/div[2]/div[@class=&quot;multiselect--active multiselect--above multiselect select-status&quot;]/div[@class=&quot;multiselect__content-wrapper&quot;]/ul[@id=&quot;listbox-null&quot;]/li[@id=&quot;null-4&quot;]/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>7f40d1f7-7338-4de1-8aa0-53fc89c80c78</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-4']/span/span</value>
      <webElementGuid>ac73ec89-04c7-4bae-bb2f-8c5088f56b08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delivered'])[1]/following::span[2]</value>
      <webElementGuid>484cbc9f-bb19-4398-85a8-f7bca71e4794</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Processing'])[1]/following::span[4]</value>
      <webElementGuid>62a12a9c-3c2b-4d84-8860-8c76c89bad55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='On Hold'])[1]/preceding::span[1]</value>
      <webElementGuid>2442541a-a357-4404-80fd-ef8c7a9bbe4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reject'])[1]/preceding::span[3]</value>
      <webElementGuid>b23255db-a67d-4e6a-b2ef-8dd77bcfffdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Return']/parent::*</value>
      <webElementGuid>45d886d9-0a0c-4244-a012-7b604a13ad82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/span/span</value>
      <webElementGuid>799fe321-84a4-46cc-a462-506ad09b3323</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Return' or . = 'Return')]</value>
      <webElementGuid>77983450-7f17-423f-88be-7bc321e5c1a4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
