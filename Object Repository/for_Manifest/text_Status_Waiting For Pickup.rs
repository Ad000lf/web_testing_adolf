<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Status_Waiting For Pickup</name>
   <tag></tag>
   <elementGuidId>75e34fc5-696c-41b7-954b-c17c9cabccaf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='null-0']/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>492beee2-18e0-400a-8fa6-d58cff1460f5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Waiting For Pickup</value>
      <webElementGuid>2435b437-ebe8-4d02-a1d5-46d719544030</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;filter-order d-flex justify-content-between&quot;]/div[@class=&quot;filter-order-form align-items-center&quot;]/div[@class=&quot;filter-group grid-tc-4&quot;]/div[2]/div[@class=&quot;multiselect--active multiselect--above multiselect select-status&quot;]/div[@class=&quot;multiselect__content-wrapper&quot;]/ul[@id=&quot;listbox-null&quot;]/li[@id=&quot;null-0&quot;]/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>3c56fc9c-a8de-4936-a06f-39d81d3941ab</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-0']/span/span</value>
      <webElementGuid>94cc72d5-54c5-4efe-9f77-a661238e0890</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manifested At'])[1]/following::span[3]</value>
      <webElementGuid>0d8bab9c-1c3d-4cdc-ad4e-64d95ec04a48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Failed Pickup'])[1]/preceding::span[1]</value>
      <webElementGuid>a3ae1573-a336-43b2-8e84-24ce3702b6d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Processing'])[1]/preceding::span[3]</value>
      <webElementGuid>8fe15917-4cfa-4623-9ffc-4b764db57f66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Waiting For Pickup']/parent::*</value>
      <webElementGuid>b0acfc42-3a84-430f-9696-a8963a175a4e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/span/span</value>
      <webElementGuid>8cdd3d10-8c20-46ba-9288-1c4562ce2b95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Waiting For Pickup' or . = 'Waiting For Pickup')]</value>
      <webElementGuid>194b328a-7747-496d-8be1-b8a5937fcdab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
