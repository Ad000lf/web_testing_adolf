<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Kuri_JNE Express</name>
   <tag></tag>
   <elementGuidId>dadd0d6a-704c-42eb-ab42-9a3b0b6ea898</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='null-2']/span/span)[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#null-2 > span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>a98a973f-6336-429e-8b9f-a91b5530915a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>JNE Express</value>
      <webElementGuid>8fbaa9dc-6b18-49b3-8666-7bd3c8984cdb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;filter-order d-flex justify-content-between&quot;]/div[@class=&quot;filter-order-form align-items-center&quot;]/div[@class=&quot;filter-group grid-tc-4&quot;]/div[3]/div[@class=&quot;multiselect--active multiselect--above multiselect select-status&quot;]/div[@class=&quot;multiselect__content-wrapper&quot;]/ul[@id=&quot;listbox-null&quot;]/li[@id=&quot;null-2&quot;]/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>d2be6591-ece8-4ea6-a201-014b97e19d21</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='null-2']/span/span)[2]</value>
      <webElementGuid>f6acde93-d550-45b1-9934-5d5a424a5a74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SAP Express'])[1]/following::span[2]</value>
      <webElementGuid>4447ed79-6c02-497f-ba6a-b935386d7fb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OExpress'])[2]/following::span[4]</value>
      <webElementGuid>39873fcf-f21e-4f74-9acf-b85465df9aab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='J&amp;T Express'])[1]/preceding::span[1]</value>
      <webElementGuid>90541872-33f0-410b-8b14-5f525e6f7df6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ninja Xpress'])[1]/preceding::span[3]</value>
      <webElementGuid>715cf52c-ddac-4845-8c7b-2301a977e702</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='JNE Express']/parent::*</value>
      <webElementGuid>020d288f-1485-42c2-8828-2da6a1ecef1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]/ul/li[3]/span/span</value>
      <webElementGuid>3ef8cf0a-71e0-4c43-8ee4-958c5eb89c59</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'JNE Express' or . = 'JNE Express')]</value>
      <webElementGuid>bb956415-9ecd-4846-baf3-5eccd3e53720</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
