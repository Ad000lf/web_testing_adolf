<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>submenu_Anggota Tim</name>
   <tag></tag>
   <elementGuidId>38348dc1-ebef-401e-b586-99035c774a43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.router-link-active.router-link-exact-active.side-nav-link.submenu > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[7]/div[2]/div[2]/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4401954f-64ec-4443-b566-e566f9dcfbf7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Anggota Tim</value>
      <webElementGuid>28bfb0b8-ab95-4278-8514-efd8bb34e898</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftside-menu-container&quot;)/div[@class=&quot;simplebar-wrapper&quot;]/div[@class=&quot;simplebar-mask&quot;]/div[@class=&quot;simplebar-offset&quot;]/div[@class=&quot;simplebar-content-wrapper d-flex justify-content-between flex-column&quot;]/div[@class=&quot;simplebar-content&quot;]/div[@class=&quot;d-flex justify-content-between flex-column&quot;]/ul[@class=&quot;side-nav&quot;]/li[@class=&quot;side-nav-item&quot;]/div[@class=&quot;side-nav-submenu&quot;]/div[2]/a[@class=&quot;router-link-active router-link-exact-active side-nav-link submenu&quot;]/span[1]</value>
      <webElementGuid>9d54c849-5f6c-4b80-ae10-7ad8626d9657</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[7]/div[2]/div[2]/a/span</value>
      <webElementGuid>33944189-aaff-40af-bbf5-b9aac14c3edc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pengaturan Akun'])[1]/following::span[1]</value>
      <webElementGuid>460a168d-07f2-4cd9-ac22-539fad2d6d1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pengaturan'])[1]/following::span[2]</value>
      <webElementGuid>4e8e5610-b27a-4b4e-9447-267f114a6056</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Template Kiriman'])[1]/preceding::span[1]</value>
      <webElementGuid>ff57ba28-9f40-4457-89db-f13bf9f8dc09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat Saya'])[1]/preceding::span[2]</value>
      <webElementGuid>defb78e1-e860-41c9-bebb-404d2e178fbe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Anggota Tim']/parent::*</value>
      <webElementGuid>db153f16-397e-492e-9e2e-7d1ba3155a9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a/span</value>
      <webElementGuid>ed18bc48-0b09-4685-9047-4e88111b8253</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Anggota Tim' or . = 'Anggota Tim')]</value>
      <webElementGuid>97fe1e76-fc18-423a-ad40-db92943326d9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
