<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>submenu_Pengaturan Akun</name>
   <tag></tag>
   <elementGuidId>0ccca49f-bdef-4c0a-b7b7-8d7b5c305628</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.router-link-active.router-link-exact-active.side-nav-link.submenu > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[7]/div[2]/div/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>24fa2848-0b85-4e06-b81f-d9c0454c45c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pengaturan Akun</value>
      <webElementGuid>7742b8ff-323e-4085-8ff9-0130591f7793</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftside-menu-container&quot;)/div[@class=&quot;simplebar-wrapper&quot;]/div[@class=&quot;simplebar-mask&quot;]/div[@class=&quot;simplebar-offset&quot;]/div[@class=&quot;simplebar-content-wrapper d-flex justify-content-between flex-column&quot;]/div[@class=&quot;simplebar-content&quot;]/div[@class=&quot;d-flex justify-content-between flex-column&quot;]/ul[@class=&quot;side-nav&quot;]/li[@class=&quot;side-nav-item&quot;]/div[@class=&quot;side-nav-submenu&quot;]/div[1]/a[@class=&quot;router-link-active router-link-exact-active side-nav-link submenu&quot;]/span[1]</value>
      <webElementGuid>6aae22db-9b1b-4b46-865a-aefeddde179b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[7]/div[2]/div/a/span</value>
      <webElementGuid>1c0ffe74-f772-4d5e-a388-789d66a0bbcc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pengaturan'])[1]/following::span[1]</value>
      <webElementGuid>b5bfaa8e-d966-4bd2-824c-e3c3e4dc9e13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tagihan'])[1]/following::span[2]</value>
      <webElementGuid>5bb30e78-f273-48d3-a5fc-fad77618eed7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Anggota Tim'])[1]/preceding::span[1]</value>
      <webElementGuid>f6d0578d-6ed2-4f72-9098-ca621870488b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Template Kiriman'])[1]/preceding::span[2]</value>
      <webElementGuid>59a4991f-a939-4679-a090-a4c5833924d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pengaturan Akun']/parent::*</value>
      <webElementGuid>59821689-fd4b-4749-8bd8-d307e519beda</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/a/span</value>
      <webElementGuid>18f101bc-daaf-4132-9b66-eac998236cb0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Pengaturan Akun' or . = 'Pengaturan Akun')]</value>
      <webElementGuid>b53b292e-4b26-419c-a851-02ab8fb98e7e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
