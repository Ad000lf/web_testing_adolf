<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>searchbox_Username</name>
   <tag></tag>
   <elementGuidId>0418cc92-cc19-4084-ad83-4284a7c21712</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-header.d-flex.gap-1.justify-content-between > div.input-group > input.form-control</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>86397006-bc24-41fd-878d-a64486e97f0d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>cbf14863-8432-4e98-8f6e-6a5d561ecbce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>515bf2f0-f942-4347-833e-7ffea0c4aca9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Cari Nama / Email</value>
      <webElementGuid>7805826d-925c-4cd8-bbc4-d74429b0d833</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Text input with segmented dropdown button</value>
      <webElementGuid>309b5bf1-aa78-4a4b-a208-dd5d9d341087</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>button-search-member</value>
      <webElementGuid>ce96246c-7eae-4b69-8d6a-1f9793a37506</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[3]/div[@class=&quot;container&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-header d-flex gap-1 justify-content-between&quot;]/div[@class=&quot;input-group&quot;]/input[@class=&quot;form-control&quot;]</value>
      <webElementGuid>083edb1e-ed0a-4ec4-863d-191f5312efb8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[3]</value>
      <webElementGuid>e81cc920-8f71-4b8e-9d79-ed4c84f2edc0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div[2]/div/div/input</value>
      <webElementGuid>c7607b0a-847e-4970-a573-65367c599fce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input</value>
      <webElementGuid>022d3e5c-3ca3-4f81-a20f-d0f1d819beb8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @placeholder = 'Cari Nama / Email']</value>
      <webElementGuid>9053dbbb-5854-4026-9137-c0ac5063b4c4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
