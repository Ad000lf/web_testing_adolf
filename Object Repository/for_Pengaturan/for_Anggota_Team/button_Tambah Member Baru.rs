<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Tambah Member Baru</name>
   <tag></tag>
   <elementGuidId>0e75c8bc-b22c-4997-87ba-699c16c8f329</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-header.d-flex.gap-1.justify-content-between > button.btn.btn-primary.btn-undefined</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div[2]/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7dae4760-482b-47c7-be13-3896ab83842b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-undefined</value>
      <webElementGuid>41bfbec5-d8a6-4cd1-be38-33575a780a88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tambah Member Baru</value>
      <webElementGuid>5a421588-7e8d-4f11-87f3-4c36bf47da5f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[3]/div[@class=&quot;container&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-header d-flex gap-1 justify-content-between&quot;]/button[@class=&quot;btn btn-primary btn-undefined&quot;]</value>
      <webElementGuid>078510a1-c4bd-4a7d-a51d-0f489918f5a7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div[2]/div/button</value>
      <webElementGuid>12df2868-e6c2-422b-a2d1-805654cc9b22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Anggota Tim'])[2]/following::button[2]</value>
      <webElementGuid>794be2c5-394b-4a6f-9e53-da6303f200d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Social Commerce'])[1]/following::button[3]</value>
      <webElementGuid>d2a66b35-182f-4622-8f9f-51b37c221dc2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username/Email'])[1]/preceding::button[1]</value>
      <webElementGuid>83f4d340-437f-47db-b77e-588aeda2ad0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Telepon'])[1]/preceding::button[1]</value>
      <webElementGuid>9b56cea0-a88e-4e01-8685-17a8b1e71039</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Tambah Member Baru']/parent::*</value>
      <webElementGuid>c14f4909-3a0e-4109-8ac5-6650f961f11d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/button</value>
      <webElementGuid>4867f85c-181c-44d7-b906-d7fbb6e388ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Tambah Member Baru' or . = 'Tambah Member Baru')]</value>
      <webElementGuid>9d258ae3-e919-425f-9adc-e24a51acf31a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
