<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Date_R1</name>
   <tag></tag>
   <elementGuidId>79a7d2ad-119e-4472-b510-0e8a65647d6c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td:nth-of-type(4)</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div[2]/div[2]/div/table/tbody/tr/td[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>82d41c66-9ae1-4528-b4a3-49d849a103a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>06-09-2023</value>
      <webElementGuid>0d92d567-6d56-4979-a6c0-3d8c60ca0a89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[3]/div[@class=&quot;container&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table&quot;]/tbody[1]/tr[1]/td[4]</value>
      <webElementGuid>3504fefa-068e-4860-a651-d7aaf39c4154</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div[2]/div[2]/div/table/tbody/tr/td[4]</value>
      <webElementGuid>c180ef96-2da9-4acf-bf7d-fb19ec197e02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::td[2]</value>
      <webElementGuid>c1c2f049-3a6c-40bd-8d22-b43d6f7097d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::td[2]</value>
      <webElementGuid>1acd05f9-47fe-4a26-b810-6216ffcd3305</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='06-09-2023']/parent::*</value>
      <webElementGuid>45ef18a2-e86a-4857-9a74-14e3413a059b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[4]</value>
      <webElementGuid>a5848975-c7b8-4fc2-91b8-f5da06e441fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '06-09-2023' or . = '06-09-2023')]</value>
      <webElementGuid>b5e0db5e-3bb0-434e-b865-c2af00c8f0cf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
