<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Customer Service</name>
   <tag></tag>
   <elementGuidId>bf99289e-1709-4003-b791-4e26051c9b05</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#null-1 > span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div/div/div[3]/ul/li[2]/span/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>4c6dec81-53f5-4cfe-bc88-647b874be329</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Customer Service</value>
      <webElementGuid>8ab44ab4-ed7f-4009-a9d6-61cdf47f42e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;form-container&quot;]/div[@class=&quot;form-container-right&quot;]/div[@class=&quot;mt-3&quot;]/div[@class=&quot;multiselect--active multiselect select-status me-2&quot;]/div[@class=&quot;multiselect__content-wrapper&quot;]/ul[@id=&quot;listbox-null&quot;]/li[@id=&quot;null-1&quot;]/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>6e7094a0-81e2-4745-90f0-d96e07189eb1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='null-1']/span/span)[2]</value>
      <webElementGuid>11b01a68-bb34-4903-97ef-8404c60642da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Admin'])[2]/following::span[2]</value>
      <webElementGuid>dd7fa19d-3c6e-4796-b179-90d9641b33cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div[3]/ul/li[2]/span/span</value>
      <webElementGuid>779e27bf-b59c-4043-9525-767380e53eaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Customer Service' or . = 'Customer Service')]</value>
      <webElementGuid>5ade1b00-d76b-4624-acdb-94571e98d05c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
