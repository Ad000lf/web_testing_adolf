<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Member berhasil ditambahkan</name>
   <tag></tag>
   <elementGuidId>d73500f4-2a05-46e0-96ab-be18390a199e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[(text() = 'Member berhasil ditambahkan.' or . = 'Member berhasil ditambahkan.')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>1c088fd2-2972-420e-9ae0-503114658b2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>notification-content</value>
      <webElementGuid>54151b61-145f-44a4-8bc0-51ab3d73475b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Member berhasil ditambahkan.</value>
      <webElementGuid>0cf798b4-1f59-4d55-913c-25c5ddb3769d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/div[@class=&quot;vue-notification-wrapper&quot;]/div[@class=&quot;vue-notification-template vue-notification success&quot;]/div[@class=&quot;notification-content&quot;]</value>
      <webElementGuid>7dfe1f58-a66c-4a79-86ac-0b8a914b41ed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Member berhasil ditambahkan.' or . = 'Member berhasil ditambahkan.')]</value>
      <webElementGuid>5b7f3f32-0b06-4ac5-9efc-3464615ce5f7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
