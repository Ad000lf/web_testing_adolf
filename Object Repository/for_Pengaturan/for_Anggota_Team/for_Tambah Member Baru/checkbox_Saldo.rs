<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>checkbox_Saldo</name>
   <tag></tag>
   <elementGuidId>1c71f93f-4bc3-4a3b-9d21-82ccb9762f91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#wallet-feature</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div[2]/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>25afd311-2f48-4910-ba59-9a8400638103</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>wallet-feature</value>
      <webElementGuid>acb79a91-8602-4650-b7ab-6de0d659c197</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-input me-1</value>
      <webElementGuid>44392b8b-185e-4d21-bb17-57b4c63c67c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>575a3893-4f2a-432c-8417-79dc3cd1f074</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;form-container&quot;]/div[@class=&quot;form-container-right&quot;]/div[@class=&quot;mt-3 overflow-scroll&quot;]/div[@class=&quot;mb-3&quot;]/input[@id=&quot;wallet-feature&quot;]</value>
      <webElementGuid>262c2850-c2df-4f2d-bc73-053fc6e4686b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='wallet-feature']</value>
      <webElementGuid>fa1d1624-a447-46ce-806e-5d3ea6139e1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[4]/div/div/div[2]/div/div[2]/div[2]/div[2]/input</value>
      <webElementGuid>3c0c7a3d-f76a-4d7e-be70-0c5b76a3c05d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/input</value>
      <webElementGuid>247be2f6-3943-4540-bbb4-28697a9b8d41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'wallet-feature' and @type = 'checkbox']</value>
      <webElementGuid>74fe39a0-e280-4feb-b95b-875ac570d921</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
