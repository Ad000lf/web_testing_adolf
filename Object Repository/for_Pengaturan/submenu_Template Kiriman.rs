<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>submenu_Template Kiriman</name>
   <tag></tag>
   <elementGuidId>4f966391-ed02-4532-b37c-09e102b40449</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div:nth-of-type(3) > a.side-nav-link.submenu > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[3]/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6559a203-4e41-4d1e-9f9a-5a1144a7f5d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Template Kiriman</value>
      <webElementGuid>57cf5376-7e39-430f-8bc9-423e3bc1e77e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftside-menu-container&quot;)/div[@class=&quot;simplebar-wrapper&quot;]/div[@class=&quot;simplebar-mask&quot;]/div[@class=&quot;simplebar-offset&quot;]/div[@class=&quot;simplebar-content-wrapper d-flex justify-content-between flex-column&quot;]/div[@class=&quot;simplebar-content&quot;]/div[@class=&quot;d-flex justify-content-between flex-column&quot;]/ul[@class=&quot;side-nav&quot;]/li[@class=&quot;side-nav-item&quot;]/div[@class=&quot;side-nav-submenu&quot;]/div[3]/a[@class=&quot;side-nav-link submenu&quot;]/span[1]</value>
      <webElementGuid>ae2d68cd-7b01-4a40-92e8-c2fc21c2b9cd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[7]/div[2]/div[3]/a/span</value>
      <webElementGuid>1fe32a2a-e43d-47f3-8765-db80e5af6c62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Anggota Tim'])[1]/following::span[1]</value>
      <webElementGuid>12d5f65a-f23e-4888-9150-cf2381ebdcfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pengaturan Akun'])[1]/following::span[2]</value>
      <webElementGuid>8c261f98-6d9d-45a0-8ead-cad2634ef5ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat Saya'])[1]/preceding::span[1]</value>
      <webElementGuid>06bac848-cff8-4263-b4cc-5ce525870e7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Akun Bank'])[1]/preceding::span[2]</value>
      <webElementGuid>3aa93caf-d5b9-45b6-993c-41085a1a0de7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Template Kiriman']/parent::*</value>
      <webElementGuid>62c06045-a863-4d21-aa49-5caa4f208c2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/a/span</value>
      <webElementGuid>78f3b280-4e41-4cd6-969c-4d3b2acf65e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Template Kiriman' or . = 'Template Kiriman')]</value>
      <webElementGuid>701689a5-13df-4abf-ad1c-55b7d599137a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
