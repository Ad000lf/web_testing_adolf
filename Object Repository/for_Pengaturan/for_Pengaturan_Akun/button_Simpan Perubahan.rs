<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Simpan Perubahan</name>
   <tag></tag>
   <elementGuidId>58980387-cf83-47d3-887b-c2d8e22f442b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.mt-3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[4]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>205c343b-4f80-466e-9037-ec503fcfae43</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary mt-3</value>
      <webElementGuid>a98c4339-4f2f-4b93-ab45-ffd8e1804456</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Simpan Perubahan </value>
      <webElementGuid>a38dfa44-8dd9-4f2f-a237-914b1fcefffe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;p-account-settings&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;py-2 text-right&quot;]/button[@class=&quot;btn btn-primary mt-3&quot;]</value>
      <webElementGuid>c68c518f-1537-498a-b13a-625ab609449e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div/div/div/div/form/div[4]/button</value>
      <webElementGuid>985c8588-dbf6-40ce-a18c-e8112d9aa10a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No. Handphone'])[1]/following::button[1]</value>
      <webElementGuid>e75e96f8-0393-49a2-b500-721bdede05e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ganti Email'])[1]/following::button[1]</value>
      <webElementGuid>f637b0af-5d8c-4354-a9cf-dd47069336d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ubah Password'])[1]/preceding::button[1]</value>
      <webElementGuid>b940f362-137f-4e89-9b87-1599c0d02bdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password Saat Ini'])[1]/preceding::button[1]</value>
      <webElementGuid>bc45f3c3-ee33-4ea4-a25e-0b9d130264b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Simpan Perubahan']/parent::*</value>
      <webElementGuid>354532e5-c9b4-4558-9833-805e1a863f05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/button</value>
      <webElementGuid>29edc6bf-ec04-465a-9aa1-eac67af7f6bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = ' Simpan Perubahan ' or . = ' Simpan Perubahan ')]</value>
      <webElementGuid>412a5d1d-36a3-484c-83b8-515747539ecb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
