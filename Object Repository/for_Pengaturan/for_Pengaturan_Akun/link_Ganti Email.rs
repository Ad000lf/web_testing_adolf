<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_Ganti Email</name>
   <tag></tag>
   <elementGuidId>50ed2082-35c8-4669-8d56-efc5f122661d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.text-info.cursor-pointer</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div/div/div/div/form/div[2]/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>05152222-0997-401e-9c3e-8209cb9c7cdc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-info cursor-pointer</value>
      <webElementGuid>8e06442c-5838-47d3-844c-54cb752af7d1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ganti Email </value>
      <webElementGuid>543524fb-3c89-4208-ac15-979b04fc046b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;p-account-settings&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;mt-3&quot;]/div[@class=&quot;d-flex justify-content-between&quot;]/div[@class=&quot;text-info cursor-pointer&quot;]</value>
      <webElementGuid>7517c801-a887-42c9-baec-77079b9fa358</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div/div/div/div/form/div[2]/div/div</value>
      <webElementGuid>68458f67-7627-4f19-9f04-506be4eb0589</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::div[1]</value>
      <webElementGuid>b544ec72-a7fc-4073-af86-982af096831c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/following::div[3]</value>
      <webElementGuid>c42044be-5a70-445b-9b80-6856c5c1fa8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No. Handphone'])[1]/preceding::div[1]</value>
      <webElementGuid>2da29080-2b75-4293-81f1-f0ff1b34e320</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Simpan Perubahan'])[1]/preceding::div[2]</value>
      <webElementGuid>52fee1f5-c073-4e44-bcb8-cbef80f89c49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ganti Email']/parent::*</value>
      <webElementGuid>a59499b2-5130-4e39-bb07-373ac6e9317b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[2]/div/div</value>
      <webElementGuid>79d3ba31-4606-41a8-9eb3-b9b61dc7d9bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Ganti Email ' or . = 'Ganti Email ')]</value>
      <webElementGuid>1ec7f88c-076c-4bc0-87c6-55c8a27cad72</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
