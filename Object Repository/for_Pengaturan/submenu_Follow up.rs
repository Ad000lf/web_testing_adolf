<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>submenu_Follow up</name>
   <tag></tag>
   <elementGuidId>abf99caa-ca49-46c3-a311-954e61174c58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div:nth-of-type(7) > a.side-nav-link.submenu > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[7]/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>892a07db-8307-46ae-b0bc-7270d2a88939</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Follow up</value>
      <webElementGuid>d8faa35c-c5df-47f6-a773-fd1e8f7b6fa1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftside-menu-container&quot;)/div[@class=&quot;simplebar-wrapper&quot;]/div[@class=&quot;simplebar-mask&quot;]/div[@class=&quot;simplebar-offset&quot;]/div[@class=&quot;simplebar-content-wrapper d-flex justify-content-between flex-column&quot;]/div[@class=&quot;simplebar-content&quot;]/div[@class=&quot;d-flex justify-content-between flex-column&quot;]/ul[@class=&quot;side-nav&quot;]/li[@class=&quot;side-nav-item&quot;]/div[@class=&quot;side-nav-submenu&quot;]/div[7]/a[@class=&quot;side-nav-link submenu&quot;]/span[1]</value>
      <webElementGuid>99c3419b-828f-424a-949e-3745602219c6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[7]/div[2]/div[7]/a/span</value>
      <webElementGuid>59d23915-382a-4f0f-b010-853f67227e9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pengaturan Toko'])[1]/following::span[1]</value>
      <webElementGuid>133a2fd7-8f7a-4adc-a21b-23adf80072a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Akun Bank'])[1]/following::span[2]</value>
      <webElementGuid>03e49bbf-4e5e-496f-b138-43525e8daaf0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pengaturan Kurir'])[1]/preceding::span[1]</value>
      <webElementGuid>46d49eb9-77f6-478c-b519-5f52b1acf7cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Third Party'])[1]/preceding::span[2]</value>
      <webElementGuid>4fa21a26-b2c9-4423-aac8-cf91bdf15df2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Follow up']/parent::*</value>
      <webElementGuid>9496f86b-611a-4fbb-ac4f-d352f21a4af9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/a/span</value>
      <webElementGuid>0b195272-4d2c-40a3-93f9-4dd9edf8addb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Follow up' or . = 'Follow up')]</value>
      <webElementGuid>d4266b0d-6b45-4457-83ac-5a6509433de3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
