<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>submenu_Pengaturan Kurir</name>
   <tag></tag>
   <elementGuidId>d4236d83-0de6-4cc4-87da-e66d9ce0926b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div:nth-of-type(8) > a.side-nav-link.submenu > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[7]/div[2]/div[8]/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>bc07f04c-5618-450c-a7e6-f6872ddac8a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pengaturan Kurir</value>
      <webElementGuid>df790841-6f7e-4148-80de-c5db63c0b7b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftside-menu-container&quot;)/div[@class=&quot;simplebar-wrapper&quot;]/div[@class=&quot;simplebar-mask&quot;]/div[@class=&quot;simplebar-offset&quot;]/div[@class=&quot;simplebar-content-wrapper d-flex justify-content-between flex-column&quot;]/div[@class=&quot;simplebar-content&quot;]/div[@class=&quot;d-flex justify-content-between flex-column&quot;]/ul[@class=&quot;side-nav&quot;]/li[@class=&quot;side-nav-item&quot;]/div[@class=&quot;side-nav-submenu&quot;]/div[8]/a[@class=&quot;side-nav-link submenu&quot;]/span[1]</value>
      <webElementGuid>31d985fa-5dbe-4a4f-96be-9587e59c83bc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[7]/div[2]/div[8]/a/span</value>
      <webElementGuid>d31ab797-f8a2-4adb-8b03-4b472b9f840e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Follow up'])[1]/following::span[1]</value>
      <webElementGuid>a9f895b5-7ee9-4f08-b786-574479d09b4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pengaturan Toko'])[1]/following::span[2]</value>
      <webElementGuid>89948f2e-22ad-4151-b35a-52deda17f297</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Third Party'])[1]/preceding::span[1]</value>
      <webElementGuid>0a3564d4-1338-47fc-8072-d7e81da9b642</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pusat Bantuan'])[1]/preceding::span[2]</value>
      <webElementGuid>2166ed3b-3512-415e-8218-9a6be84375e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pengaturan Kurir']/parent::*</value>
      <webElementGuid>9c99e809-ddb8-4ca7-8262-da01fcd15c9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/a/span</value>
      <webElementGuid>3a0a3841-4ef4-4c25-95a3-f19b8c52f864</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Pengaturan Kurir' or . = 'Pengaturan Kurir')]</value>
      <webElementGuid>e111c3e6-7496-4ad2-8b85-b2616ed1b71f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
