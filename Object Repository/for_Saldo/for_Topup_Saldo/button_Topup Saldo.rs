<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Topup Saldo</name>
   <tag></tag>
   <elementGuidId>f7b3ac09-eab6-4e14-af8c-72dce37757a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[3]/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>95e15427-72ad-4719-b94f-0c3e94e16a4a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>09a771b9-89d1-4b1e-bb60-86db4da3902e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Topup Saldo </value>
      <webElementGuid>111a9234-b595-4ef5-82ff-a7ef05a13412</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;p-ballance container py-3&quot;]/div[@class=&quot;mb-3 d-flex&quot;]/div[@class=&quot;d-flex gap-1 ballance-button-actions&quot;]/button[@class=&quot;btn btn-primary&quot;]</value>
      <webElementGuid>89750264-759f-4478-ad6c-eb8d68d42c42</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div/button</value>
      <webElementGuid>9efefa55-2838-43c6-94dd-7e0411227cb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Social Commerce'])[1]/following::button[2]</value>
      <webElementGuid>7dca2fd0-aa60-4488-9bcf-dac9ab81715c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Acol'])[1]/following::button[2]</value>
      <webElementGuid>d1550f81-f273-4421-a43d-ba956df5cbd1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ajukan Penarikan'])[1]/preceding::button[1]</value>
      <webElementGuid>e089dca1-6e0e-4bd5-b3c5-b30a4c8be49f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Penghasilan COD &amp; Non COD'])[1]/preceding::button[3]</value>
      <webElementGuid>82da2211-43bc-400f-a759-a966b74020df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Topup Saldo']/parent::*</value>
      <webElementGuid>d33af12b-b455-414c-a595-83eb944cded3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/button</value>
      <webElementGuid>1d23de1c-3ce0-4951-8ad3-9a4a53980db2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = ' Topup Saldo ' or . = ' Topup Saldo ')]</value>
      <webElementGuid>5cecbbc5-3ad2-450c-a74b-82a227166030</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
