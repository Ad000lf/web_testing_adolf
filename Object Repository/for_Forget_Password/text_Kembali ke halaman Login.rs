<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Kembali ke halaman Login</name>
   <tag></tag>
   <elementGuidId>8aa2f976-8f5b-4ec3-8eba-14bd4ea6cd9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//footer/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>footer.footer-alt.mt-3 > p.text-muted</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>5e4526fb-69f6-49d4-be3b-8e0fdd6b1735</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-muted</value>
      <webElementGuid>e0ebfa6b-29b2-496b-b07d-ce4860742e9b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Kembali ke halaman Login</value>
      <webElementGuid>1b4be36f-17a5-4c93-948b-242a36c5ec6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;layout-auth&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/footer[@class=&quot;footer-alt mt-3&quot;]/p[@class=&quot;text-muted&quot;]</value>
      <webElementGuid>9011df48-833d-4e06-9190-f24edec6f36b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/footer/p</value>
      <webElementGuid>468b630c-5bcb-43c3-bbe3-b54442f4f541</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reset Password'])[2]/following::p[1]</value>
      <webElementGuid>1d3c87e8-2f13-48df-bf11-d3128cdfb7c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::p[1]</value>
      <webElementGuid>de4058d1-7c99-4226-abe3-58697b7ed06b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::p[1]</value>
      <webElementGuid>9387276b-eb10-4ef1-95c4-1bd2fdfac6e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kembali ke halaman']/parent::*</value>
      <webElementGuid>4e87dcad-8133-4578-8830-ac787cffed6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//footer/p</value>
      <webElementGuid>8ac01019-cafd-4958-b483-5bbae7386c81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = ' Kembali ke halaman Login' or . = ' Kembali ke halaman Login')]</value>
      <webElementGuid>2b1c0121-e736-4568-968f-f7ba1b6892b5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
