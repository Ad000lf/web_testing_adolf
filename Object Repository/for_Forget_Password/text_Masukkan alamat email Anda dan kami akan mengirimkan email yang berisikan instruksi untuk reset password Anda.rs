<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Masukkan alamat email Anda dan kami akan mengirimkan email yang berisikan instruksi untuk reset password Anda</name>
   <tag></tag>
   <elementGuidId>ed58762f-d6b3-4c97-9c4c-830e3949d0fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/div/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.text-muted.mb-3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>7367b5aa-a1ed-4e40-a2ca-23791a21eb8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-muted mb-3</value>
      <webElementGuid>9fd59902-c108-4911-b634-636a570a93e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Masukkan alamat email Anda dan kami akan mengirimkan email yang berisikan instruksi untuk reset password Anda. </value>
      <webElementGuid>765b6ba5-3cfa-462f-833a-68a93db6c0c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;layout-auth&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[2]/div[1]/p[@class=&quot;text-muted mb-3&quot;]</value>
      <webElementGuid>1018de05-2efa-4fd7-bb56-a1d8763cdba2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/p</value>
      <webElementGuid>2053cc36-7bb8-48a3-b0e4-bc8d74108ac1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reset Password'])[1]/following::p[1]</value>
      <webElementGuid>f96bc88b-7d90-48d8-b0e3-c0580ed26079</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/preceding::p[1]</value>
      <webElementGuid>651a1852-f037-40b5-a54b-b847b2c1fe11</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reset Password'])[2]/preceding::p[1]</value>
      <webElementGuid>9a5fc14d-37f6-4e8a-aa7f-ff8ec71bab32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Masukkan alamat email Anda dan kami akan mengirimkan email yang berisikan instruksi untuk reset password Anda.']/parent::*</value>
      <webElementGuid>49ea3704-44e9-4c7e-bfb5-f90a0cd07f19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>44c3188b-24d6-4ed2-b74e-0e116ed0c947</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = ' Masukkan alamat email Anda dan kami akan mengirimkan email yang berisikan instruksi untuk reset password Anda. ' or . = ' Masukkan alamat email Anda dan kami akan mengirimkan email yang berisikan instruksi untuk reset password Anda. ')]</value>
      <webElementGuid>d572b049-fd02-44eb-940d-1b79b3a365fd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
