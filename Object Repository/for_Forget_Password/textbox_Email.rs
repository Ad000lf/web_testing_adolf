<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textbox_Email</name>
   <tag></tag>
   <elementGuidId>7c0b0c83-c251-48d0-8687-45a363c48761</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[1]/div/div/div[2]/div/div/form/div[1]/input</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;email&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3abbbe55-c1e8-448f-9347-2186286029c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>fc7c5b8d-b83a-4498-9c2c-f2dbbea562ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>9cc5a282-524a-483f-a08f-81e06b12dc3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter your email</value>
      <webElementGuid>c4d11798-212a-4093-9443-36c5ecd3b04e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>email</value>
      <webElementGuid>9a443310-3416-4ae9-9624-3bdd9a490e4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;layout-auth&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[2]/div[1]/div[1]/form[1]/div[@class=&quot;mb-3&quot;]/input[@class=&quot;form-control&quot;]</value>
      <webElementGuid>8ac2d42f-5881-4fdf-83b9-3dedc1b07f18</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='email']</value>
      <webElementGuid>39480a30-28fc-4819-b8d2-127b85f0c2a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div/form/div/input</value>
      <webElementGuid>628b7232-5cd0-4ec6-8b29-4fd6997d0f8f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>29b96cf2-6a68-4d02-b5ad-e373019aa04c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'email' and @placeholder = 'Enter your email' and @name = 'email']</value>
      <webElementGuid>7e230e1e-9a1b-4fc1-9671-910ae281168c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
