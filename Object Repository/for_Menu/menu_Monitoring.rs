<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>menu_Monitoring</name>
   <tag></tag>
   <elementGuidId>e757273f-98b6-46e8-a9a0-934a5521035b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[5]/a/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>754dddee-9bd9-4001-968c-a16060f6f67c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Monitoring</value>
      <webElementGuid>42087e80-123a-449f-ba86-5d301fb1c032</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftside-menu-container&quot;)/div[@class=&quot;simplebar-wrapper&quot;]/div[@class=&quot;simplebar-mask&quot;]/div[@class=&quot;simplebar-offset&quot;]/div[@class=&quot;simplebar-content-wrapper d-flex justify-content-between flex-column&quot;]/div[@class=&quot;simplebar-content&quot;]/div[@class=&quot;d-flex justify-content-between flex-column&quot;]/ul[@class=&quot;side-nav&quot;]/li[@class=&quot;side-nav-item&quot;]/a[@class=&quot;side-nav-link&quot;]/div[1]/span[1]</value>
      <webElementGuid>266946ff-22a5-4967-87c9-6433e2594eb8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[5]/a/div/span</value>
      <webElementGuid>e231412a-3c6a-4ade-99e8-d82c8d7248ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Saldo'])[1]/following::span[1]</value>
      <webElementGuid>d47da16b-7540-4594-b518-d9d76e40b58f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Kiriman'])[1]/following::span[2]</value>
      <webElementGuid>d3184a2d-a0b4-4987-aac7-7e9e3b32b663</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tagihan'])[1]/preceding::span[1]</value>
      <webElementGuid>bf6a330b-2cba-4fa2-858d-9bf5bf18d813</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pengaturan'])[1]/preceding::span[2]</value>
      <webElementGuid>2be4c083-c0a4-4858-aa67-83594bffac26</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Monitoring']/parent::*</value>
      <webElementGuid>27bc5cc3-9667-4f07-b885-6351fde28dae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/a/div/span</value>
      <webElementGuid>03c75a7d-74a6-4b5f-ab60-4ab3ec7183f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Monitoring' or . = 'Monitoring')]</value>
      <webElementGuid>6123db36-9c04-4a86-a13f-3103421b52cf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
