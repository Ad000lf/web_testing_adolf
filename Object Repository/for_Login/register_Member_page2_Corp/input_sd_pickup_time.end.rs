<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_sd_pickup_time.end</name>
   <tag></tag>
   <elementGuidId>d21b5c94-aaca-40d8-bba6-60f182f2802e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='pickup_time.end']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;pickup_time.end&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f8e50904-b3f1-40e4-bdf8-9c878b9f16d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>pickup_time.end</value>
      <webElementGuid>fcf8dad4-dd28-4e52-832a-f72f4f0359a9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>time</value>
      <webElementGuid>69201180-cc33-4568-8822-23934cde7047</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>ec5266e0-1a91-4a60-a1ff-7255a7aa0f27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-xl modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-store p-4&quot;]/div[@class=&quot;onboard-store--form&quot;]/form[1]/div[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;d-flex gap-2 align-items-center&quot;]/input[@class=&quot;form-control&quot;]</value>
      <webElementGuid>dfbc1842-5afb-445f-8084-7a6b03bc9b9f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='pickup_time.end']</value>
      <webElementGuid>3624411c-3d7d-42a2-a70b-ca05dad5ef34</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div[2]/div/div/form/div/div[2]/div/div/div/input[2]</value>
      <webElementGuid>e420c096-84fc-4360-9a37-dbf350bbe9c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/input[2]</value>
      <webElementGuid>faecc7fc-b4dc-429a-9f39-f0e6fa59aae5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'pickup_time.end' and @type = 'time']</value>
      <webElementGuid>b49ffded-5c95-4e0f-96f3-af43992c17f5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
