<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>seelect_DKI Jakarta, Kota Jakarta Timur, Cakung</name>
   <tag></tag>
   <elementGuidId>acec8f25-8a91-4037-81ff-50c9c3c73178</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;null-0&quot;]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option--selected.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>d53b50db-ac21-4681-8e14-9f108b684876</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DKI Jakarta, Kota Jakarta Timur, Cakung</value>
      <webElementGuid>09b1aee8-9372-450f-83b5-9c375ec5103d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;null-0&quot;)/span[@class=&quot;multiselect__option--highlight multiselect__option--selected multiselect__option&quot;]/span[1]</value>
      <webElementGuid>18d13868-b263-436c-aca6-1dff66bbfe67</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-0']/span/span</value>
      <webElementGuid>c148f6cd-5db2-4810-a32d-5352169c249f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lokasi Kota / Kecamatan'])[1]/following::span[2]</value>
      <webElementGuid>93440781-407c-4ced-9fa5-8779078ad995</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat Lengkap'])[1]/following::span[2]</value>
      <webElementGuid>78e998a8-74a7-474f-babc-0e85fdd68f2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='DKI Jakarta, Kota Jakarta Timur, Cakung']/parent::*</value>
      <webElementGuid>c5765c44-aa3c-4635-b3b7-c7452d3bc87b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/span/span</value>
      <webElementGuid>91ac4a50-b6f1-42d8-8d13-6924d5a5f2ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'DKI Jakarta, Kota Jakarta Timur, Cakung' or . = 'DKI Jakarta, Kota Jakarta Timur, Cakung')]</value>
      <webElementGuid>5815c5c9-866f-40a5-8033-3875dd3d250c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
