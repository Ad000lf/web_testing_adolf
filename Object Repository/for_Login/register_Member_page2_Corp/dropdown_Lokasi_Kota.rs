<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_Lokasi_Kota</name>
   <tag></tag>
   <elementGuidId>7f145b60-25c3-49af-bf8c-5962282dcb29</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div[2]/div/div/div/div[2]/div/div/form/div/div[2]/div/div[4]/div[2]/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.multiselect--active.multiselect--above.multiselect > div.multiselect__tags</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>22231713-5c01-4fe4-94c2-91fc6ce2b1f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>multiselect__tags</value>
      <webElementGuid>6ed06865-2115-4608-91d7-b4d0e476abf0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-xl modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-store p-4&quot;]/div[@class=&quot;onboard-store--form&quot;]/form[1]/div[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;multiselect--active multiselect--above multiselect&quot;]/div[@class=&quot;multiselect__tags&quot;]</value>
      <webElementGuid>fb6a53ad-dcc7-4002-975c-38cb94546c31</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div[2]/div/div/form/div/div[2]/div/div[4]/div[2]/div[2]</value>
      <webElementGuid>910273aa-7844-41e5-909c-fd1a2b41d6bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lokasi Kota / Kecamatan'])[1]/following::div[3]</value>
      <webElementGuid>da15c10b-3988-4adb-b160-48e0b60c80e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat'])[1]/following::div[5]</value>
      <webElementGuid>38a540c6-c082-4f44-86b6-4ab680477916</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div[2]</value>
      <webElementGuid>6972c30b-a4dd-409f-98bd-b108ee13bfe2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
