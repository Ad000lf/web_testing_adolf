<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Mingguan</name>
   <tag></tag>
   <elementGuidId>6aad288f-f953-455e-b9e8-ebe9ee14ab08</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='null-1']/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1dc779d0-2392-4728-a467-d8b7fe46c595</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>multiselect__option--highlight multiselect__option</value>
      <webElementGuid>8836bd83-00d5-44ea-b456-57c7db3a8bc9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-select</name>
      <type>Main</type>
      <value>Press enter to select</value>
      <webElementGuid>d97c99dc-5cf2-4006-899e-a1ae26c43998</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-selected</name>
      <type>Main</type>
      <value>Selected</value>
      <webElementGuid>253afaab-7379-4657-bedc-1d47846950dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-deselect</name>
      <type>Main</type>
      <value>Press enter to remove</value>
      <webElementGuid>87f578ce-80d7-4e63-924c-6dd9b5fa457e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Mingguan</value>
      <webElementGuid>807ea0f2-bfb7-4ebb-9b4c-cc3f5b889bce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;null-1&quot;)/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]</value>
      <webElementGuid>ff90e595-5eac-4ea5-b896-d7eb6ef4b600</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-1']/span</value>
      <webElementGuid>3e51ec30-d144-4840-accb-e6305a054aa7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bulanan'])[1]/following::span[1]</value>
      <webElementGuid>ea0b6c02-babb-4710-8d44-5865f484ad3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harian'])[1]/preceding::span[2]</value>
      <webElementGuid>bd63f92c-ba34-4fe2-8987-99480b710c13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/span</value>
      <webElementGuid>5cdee566-e1cd-4581-ab8e-47e0f5c9a62a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Mingguan' or . = 'Mingguan')]</value>
      <webElementGuid>20243dd8-2487-4020-b63c-9d52751bdd41</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
