<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Yuk ikuti langkah dibawah ini untuk melengkapi data toko kamu</name>
   <tag></tag>
   <elementGuidId>5c335da3-b07d-42cf-be30-9fbf5d000774</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div/div/div[1]/div/div/div[1]/h4</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h4.mt-3.fw-light</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'Yuk ikuti langkah dibawah ini untuk melengkapi data toko kamu' or . = 'Yuk ikuti langkah dibawah ini untuk melengkapi data toko kamu')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>6ed84193-5322-4754-a646-bc01f0332128</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mt-3 fw-light</value>
      <webElementGuid>91407dc3-7ffb-4956-a5c9-8161d74ad680</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Yuk ikuti langkah dibawah ini untuk melengkapi data toko kamu</value>
      <webElementGuid>caf22f8e-a1de-4f92-9fcb-c329cc4396e2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-store p-4&quot;]/div[@class=&quot;text-center&quot;]/h4[@class=&quot;mt-3 fw-light&quot;]</value>
      <webElementGuid>57e54053-5b0e-4106-bfb7-e81bf94291e7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/div/h4</value>
      <webElementGuid>2a0bc6f2-21e6-4afc-b9e5-786897430972</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Selamat Datang, Acol!'])[1]/following::h4[1]</value>
      <webElementGuid>99e5cdd4-3164-477d-b0e0-de256730d2ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/following::h4[1]</value>
      <webElementGuid>e250fca0-b2a5-4b19-946f-4d166b260028</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi Toko'])[1]/preceding::h4[1]</value>
      <webElementGuid>b6199051-5190-43d3-8042-850ce2375f9f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama Toko'])[1]/preceding::h4[2]</value>
      <webElementGuid>53f7c56f-5391-4412-8fac-d6f85ec442b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Yuk ikuti langkah dibawah ini untuk melengkapi data toko kamu']/parent::*</value>
      <webElementGuid>5d431d2b-4408-4f8e-b3cd-def923a4dde8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h4</value>
      <webElementGuid>a9e9a45b-8126-4325-8bb1-6da9b14221ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = 'Yuk ikuti langkah dibawah ini untuk melengkapi data toko kamu' or . = 'Yuk ikuti langkah dibawah ini untuk melengkapi data toko kamu')]</value>
      <webElementGuid>59dce51a-018d-4622-bad7-4d7b46b80e3e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
