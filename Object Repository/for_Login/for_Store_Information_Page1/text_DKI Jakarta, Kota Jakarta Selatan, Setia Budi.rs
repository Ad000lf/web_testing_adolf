<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_DKI Jakarta, Kota Jakarta Selatan, Setia Budi</name>
   <tag></tag>
   <elementGuidId>3da01562-ab28-4025-8387-bfd7e04a4ee9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option--selected.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5eac3187-9b0e-471b-b4a4-bc91eace711a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DKI Jakarta, Kota Jakarta Selatan, Setia Budi</value>
      <webElementGuid>0442a4bf-4e27-4613-9b8e-2d31ac57e685</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;null-0&quot;)/span[@class=&quot;multiselect__option--highlight multiselect__option--selected multiselect__option&quot;]/span[1]</value>
      <webElementGuid>9f37d890-e7db-45a2-a209-9841ed950d91</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-0']/span/span</value>
      <webElementGuid>d0065df7-0462-47cc-ba0f-644ea838e86c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lokasi Kota / Kecamatan'])[1]/following::span[2]</value>
      <webElementGuid>d208d95e-ddf3-4e79-8e47-2cd18646459b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat'])[1]/following::span[2]</value>
      <webElementGuid>9282b756-7ded-493c-b425-96c163db4210</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='DKI Jakarta, Kota Jakarta Selatan, Setia Budi']/parent::*</value>
      <webElementGuid>4b8c61ec-1c2c-44f7-af31-bed0b118f405</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/span/span</value>
      <webElementGuid>809bb188-8780-45db-9cbc-f030cc7bed46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'DKI Jakarta, Kota Jakarta Selatan, Setia Budi' or . = 'DKI Jakarta, Kota Jakarta Selatan, Setia Budi')]</value>
      <webElementGuid>0d9dc98b-d46e-4a29-af40-5b495a1699ac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
