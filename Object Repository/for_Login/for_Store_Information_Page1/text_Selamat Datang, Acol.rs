<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Selamat Datang, Acol</name>
   <tag></tag>
   <elementGuidId>0e167e13-4c11-4af5-a88c-6341b6c720eb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div/div/div/div/div/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.text-center > h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>2dafaa83-f4ab-439c-a688-6875c268face</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Selamat Datang, Acol!</value>
      <webElementGuid>509bb2b8-f122-4ee7-91ad-626c6ce56cf4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-store p-4&quot;]/div[@class=&quot;text-center&quot;]/h3[1]</value>
      <webElementGuid>4888bc18-a479-4ccc-81f2-9733c36eca47</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/div/h3</value>
      <webElementGuid>8156b7b7-3c26-4e5d-8231-b6554d769bd0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/following::h3[1]</value>
      <webElementGuid>63dc4bb4-4aa5-4f11-a98d-a70bbb28cbc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak Ada Progres'])[1]/following::h3[2]</value>
      <webElementGuid>064396a0-77f3-455c-b978-6209e6c87a7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Yuk ikuti langkah dibawah ini untuk melengkapi data toko kamu'])[1]/preceding::h3[1]</value>
      <webElementGuid>82057bb4-3b64-4f3b-a702-34a19237f7ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi Toko'])[1]/preceding::h3[1]</value>
      <webElementGuid>f6c77aa8-7fb7-49d8-8fd3-f32529ac60ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Selamat Datang, Acol!']/parent::*</value>
      <webElementGuid>b11ee37b-b415-4e17-b18f-752ce6587811</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/h3</value>
      <webElementGuid>5b2a0211-59d6-439c-927f-ea552354c29d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Selamat Datang, Acol!' or . = 'Selamat Datang, Acol!')]</value>
      <webElementGuid>aa535861-bd79-437b-a59d-8bdc603e727a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
