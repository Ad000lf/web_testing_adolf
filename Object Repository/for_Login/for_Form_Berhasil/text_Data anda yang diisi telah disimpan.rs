<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Data anda yang diisi telah disimpan</name>
   <tag></tag>
   <elementGuidId>3ca68d01-3665-4d05-8fdd-6d4883aea25a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div[2]/div/div/div/div/div/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.onboard-store.p-4.text-center > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>e37acaea-9a90-42fe-acf1-05516481efc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Data anda yang diisi telah disimpan</value>
      <webElementGuid>59183118-7712-4e30-87dc-f0fab2942b10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-md modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-store p-4 text-center&quot;]/p[1]</value>
      <webElementGuid>d631c766-a8f7-4354-9e8c-6b88749d2f09</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/p</value>
      <webElementGuid>b81ee88a-f23f-4622-b94f-f2664c435cd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Berhasil'])[1]/following::p[1]</value>
      <webElementGuid>929800cd-dae6-491c-a9fd-e185500b2d8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/following::p[1]</value>
      <webElementGuid>9fe84b4d-676a-4697-93d5-8e44ddc111cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ya, Mengerti'])[1]/preceding::p[1]</value>
      <webElementGuid>d6cdba77-6f0a-408e-9c26-959490bc6f51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::p[1]</value>
      <webElementGuid>4d12c7bd-64d9-4247-b96d-13b2aac529aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Data anda yang diisi telah disimpan']/parent::*</value>
      <webElementGuid>e603dde0-fd5f-4f6f-9987-7cb3fd9a10a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/p</value>
      <webElementGuid>08c68c34-4b51-4470-9747-135bae683b45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Data anda yang diisi telah disimpan' or . = 'Data anda yang diisi telah disimpan')]</value>
      <webElementGuid>0a671dfd-fcdb-4459-b253-27c0c3f74193</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
