<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_OnBoarding Registrasi Member</name>
   <tag></tag>
   <elementGuidId>cbabc85d-62e9-4f72-8d89-aa6fa7875cca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div[2]/div/div/div/h4</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-body.pb-3 > h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>076d1864-1ff6-4c0e-a66b-fb6567f2bf3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OnBoarding Registrasi Member</value>
      <webElementGuid>125a1e54-fc2b-4cf5-b988-2837606cc4e6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-xl modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/h4[1]</value>
      <webElementGuid>36ef531f-3485-4295-bcc2-5f97ec8e57df</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/h4</value>
      <webElementGuid>bf0a722c-2223-48d3-81e8-05b27df33e6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/following::h4[1]</value>
      <webElementGuid>e2877848-52c1-474e-a250-6f83a0f4d0e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak Ada Progres'])[1]/following::h4[1]</value>
      <webElementGuid>ab6f8c84-4092-45cf-ba7c-f344bfb8005c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama Perusahaan'])[1]/preceding::h4[1]</value>
      <webElementGuid>6a5eeb2d-ae43-4ecb-9558-e2950ae83579</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama Owner'])[1]/preceding::h4[1]</value>
      <webElementGuid>b3623259-5624-45be-ba55-8cd5d27f8d16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='OnBoarding Registrasi Member']/parent::*</value>
      <webElementGuid>cbd949e5-8627-4c12-a4bf-6eed2d58c114</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/h4</value>
      <webElementGuid>a6e7dfb7-dc15-4b54-9424-00869e6daace</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = 'OnBoarding Registrasi Member' or . = 'OnBoarding Registrasi Member')]</value>
      <webElementGuid>69fbe977-1a46-44b2-a8fd-11cc1da8d2cc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
