<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_DKI Jakarta, Kota Jakarta Selatan, Setia Budi</name>
   <tag></tag>
   <elementGuidId>8caddf1f-acf4-4ab3-99bd-4c128340cc58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ae61c886-adf7-4f9b-b6d9-de58a6238610</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DKI Jakarta, Kota Jakarta Selatan, Setia Budi</value>
      <webElementGuid>2a1328fd-df9d-460f-acf3-ab14d7977f46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;null-0&quot;)/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>e5e0eead-5349-495c-bc3f-1ee2a805c3bf</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-0']/span/span</value>
      <webElementGuid>5794637e-bd43-46d6-95cf-d270f5cd975b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lokasi Kota / Kecamatan'])[1]/following::span[2]</value>
      <webElementGuid>f4fa9b75-9d68-48c5-acf9-baa6c34e5204</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat Lengkap'])[1]/following::span[2]</value>
      <webElementGuid>2e46e24b-11aa-475d-8f32-3a4ffc57b209</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='DKI Jakarta, Kota Jakarta Selatan, Setia Budi']/parent::*</value>
      <webElementGuid>582f1d84-0066-4ea2-8666-0c88af4008a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/span/span</value>
      <webElementGuid>de89283e-5cad-4e5a-a936-e65f1a25ab19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'DKI Jakarta, Kota Jakarta Selatan, Setia Budi' or . = 'DKI Jakarta, Kota Jakarta Selatan, Setia Budi')]</value>
      <webElementGuid>3d6eca9b-ceb6-4d4b-ba6f-93d22cbeee3f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
