<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Pilih atau tarik file ke sini</name>
   <tag></tag>
   <elementGuidId>2d5dce6f-2e7b-4727-927c-c9e6b1e109cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div[2]/div/div/div/div/div/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.dnd-uploader > div > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f7335358-e673-4c47-ab10-d3766fdf47ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih atau tarik file ke sini</value>
      <webElementGuid>22cc8119-9b1a-4f3e-a9e7-b95cc4793ff2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-ktp p-4&quot;]/div[1]/div[@class=&quot;dnd-uploader&quot;]/div[1]/div[1]</value>
      <webElementGuid>b6aa1f7b-cedf-49cd-970b-6b9f13302b89</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/div/div/div/div</value>
      <webElementGuid>4a071ea6-7079-4273-be27-7244bc35d63d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Diri'])[1]/following::div[4]</value>
      <webElementGuid>adeecab0-5858-4b62-b0fa-d5122bccd5d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/following::div[11]</value>
      <webElementGuid>6cbf493a-781a-44fd-a9ec-04d0a9725f0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ukuran file maksimal 2MB'])[1]/preceding::div[1]</value>
      <webElementGuid>34055fd0-5c93-4724-9557-f5998221d019</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kode Kupon'])[1]/preceding::div[2]</value>
      <webElementGuid>14307884-0492-4087-9a3d-7bc0872abfce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pilih atau tarik file ke sini']/parent::*</value>
      <webElementGuid>29999b8f-3892-4f79-9f81-94e0647096ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div/div/div</value>
      <webElementGuid>f7d3a704-1351-4a90-ab35-bfbd8e96856c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Pilih atau tarik file ke sini' or . = 'Pilih atau tarik file ke sini')]</value>
      <webElementGuid>02740c0c-8a5b-41d6-802c-e2141f294141</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
