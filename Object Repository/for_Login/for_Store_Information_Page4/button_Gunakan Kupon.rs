<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Gunakan Kupon</name>
   <tag></tag>
   <elementGuidId>c9b1dd34-ca09-4f8b-9f67-aef966cfb705</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[3]/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.btn-large.button.w-25</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>1831a558-c952-4bd9-97cc-d42616dcdb8d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary btn-large button w-25</value>
      <webElementGuid>67ed8bd1-c1d2-42ce-b4d6-f159eb3f52da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Gunakan Kupon</value>
      <webElementGuid>9b3489d6-42ef-4cf6-bace-eba4b3380ed4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-ktp p-4&quot;]/div[@class=&quot;d-flex gap-1 mb-1 w-100 justify-content-between&quot;]/button[@class=&quot;btn btn-primary btn-large button w-25&quot;]</value>
      <webElementGuid>9af78768-5836-4175-81ca-cd6ddb1c31a5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/div[3]/button</value>
      <webElementGuid>99887922-28c9-4310-9983-c0b9a9800383</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='onboarding selesai.'])[1]/following::button[1]</value>
      <webElementGuid>d90bae11-6072-453d-bd88-8bdb0ad951b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='digunakan'])[1]/following::button[1]</value>
      <webElementGuid>dcafa396-b7ed-4c6a-bbbc-93e017a195b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukkan kode kupon anda jika memiliki, jika tidak anda bisa mengosongkan kode ini!'])[1]/preceding::button[1]</value>
      <webElementGuid>635f44f6-1269-43ba-b6de-75370572c8d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Selanjutnya'])[1]/preceding::button[1]</value>
      <webElementGuid>843407d5-f75a-4d2e-ac0c-121ae4c0a328</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Gunakan Kupon']/parent::*</value>
      <webElementGuid>801ea465-0fd3-4e70-82ef-2c68a95de576</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button</value>
      <webElementGuid>babf83b0-24e8-4658-b278-a3f40ed9d01e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Gunakan Kupon' or . = 'Gunakan Kupon')]</value>
      <webElementGuid>c0c6f605-75e5-4751-b977-14e071d53f3e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
