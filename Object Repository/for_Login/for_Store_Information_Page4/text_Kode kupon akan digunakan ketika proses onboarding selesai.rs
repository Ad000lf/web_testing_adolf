<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Kode kupon akan digunakan ketika proses onboarding selesai</name>
   <tag></tag>
   <elementGuidId>6d161cc9-6fcc-476e-84dd-eb399f7608de</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div[2]/div/div/div/div/div/div[2]/ul/li[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.callout.callout-info > ul > li:nth-of-type(2)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>c6d4e03f-0bf6-4837-98f2-ea65230425aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Kode kupon akan digunakan ketika proses onboarding selesai.</value>
      <webElementGuid>33183abe-c503-4ebf-9ba8-1b73a07742cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-ktp p-4&quot;]/div[@class=&quot;callout callout-info&quot;]/ul[1]/li[2]</value>
      <webElementGuid>611509cf-c7d5-4f0d-a950-c24716a653bb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/div[2]/ul/li[2]</value>
      <webElementGuid>492b0157-94f2-439d-a6fd-a957a8b0eea6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='mengisi'])[1]/following::li[1]</value>
      <webElementGuid>c3e91c75-0c5c-4992-9195-fbe7c90df637</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kode kupon akan']/parent::*</value>
      <webElementGuid>879e06d9-1575-45f0-92b8-815457a1ce05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[2]</value>
      <webElementGuid>0b1d9604-5826-4368-b627-df145cdfc712</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = ' Kode kupon akan digunakan ketika proses onboarding selesai.' or . = ' Kode kupon akan digunakan ketika proses onboarding selesai.')]</value>
      <webElementGuid>a7f5ebc4-d4bf-4fee-9a59-9162bd1078dc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
