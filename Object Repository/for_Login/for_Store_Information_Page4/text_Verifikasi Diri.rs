<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Verifikasi Diri</name>
   <tag></tag>
   <elementGuidId>91ea64e6-7149-4562-a9bc-e1f427041d94</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//h4</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>b7e897f7-df0c-44e5-9a87-e36cb97b4474</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Verifikasi Diri</value>
      <webElementGuid>961ed812-0038-42f8-abaf-efc0aac6adc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-ktp p-4&quot;]/h4[1]</value>
      <webElementGuid>8ff3855b-9176-4279-9c33-787e6fdc6da0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/h4</value>
      <webElementGuid>b7627db6-0de4-489c-a65a-36e5c029ef8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/following::h4[1]</value>
      <webElementGuid>33612c5a-e956-4400-b53a-db3ab0ad4cd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak Ada Progres'])[1]/following::h4[1]</value>
      <webElementGuid>8b13d947-52f5-4f6b-afbb-13b0627b2ca3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih atau tarik file ke sini'])[1]/preceding::h4[1]</value>
      <webElementGuid>062629e2-e695-4b20-8242-46a5a39eff55</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ukuran file maksimal 2MB'])[1]/preceding::h4[1]</value>
      <webElementGuid>a758c4af-d0f4-4f5a-9134-d41bd957281a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Verifikasi Diri']/parent::*</value>
      <webElementGuid>04d804d9-bd79-497d-8f27-e69042e8c857</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h4</value>
      <webElementGuid>2c90ce99-44d5-4c07-820d-afad1f048389</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = 'Verifikasi Diri' or . = 'Verifikasi Diri')]</value>
      <webElementGuid>0366bf0a-3c6c-4731-8b54-1af57cf80cc1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
