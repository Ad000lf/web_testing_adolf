<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Pastikan sudah mengisi kode kupon jika ingin mendapatkan keuntungan dari kupon tersebut</name>
   <tag></tag>
   <elementGuidId>91f40853-5c16-4475-89c2-65ac4a670c55</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div[2]/div/div/div/div/div/div[2]/ul/li</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.callout.callout-info > ul > li</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>0160ebc6-5d79-4cb2-b68d-340bf3a71182</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Pastikan sudah mengisi kode kupon jika ingin mendapatkan keuntungan dari kupon tersebut. </value>
      <webElementGuid>6100c17d-6332-462a-9049-69a4a9918962</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-ktp p-4&quot;]/div[@class=&quot;callout callout-info&quot;]/ul[1]/li[1]</value>
      <webElementGuid>257697ba-995e-44dc-bfd3-65a0a92b1283</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/div[2]/ul/li</value>
      <webElementGuid>3a513b31-c6ef-42ec-811e-f4e320adeb2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kode Kupon'])[1]/following::li[1]</value>
      <webElementGuid>4c793efd-6ab7-415e-ad0c-47bf281073d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ukuran file maksimal 2MB'])[1]/following::li[1]</value>
      <webElementGuid>b0817828-9dc3-493d-ac67-38d3838ff496</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pastikan sudah']/parent::*</value>
      <webElementGuid>7080d59d-d7a4-43a5-9848-64416ee0fa7f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div[2]/ul/li</value>
      <webElementGuid>d525e455-b1a9-48ce-9670-80315f84e4ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = ' Pastikan sudah mengisi kode kupon jika ingin mendapatkan keuntungan dari kupon tersebut. ' or . = ' Pastikan sudah mengisi kode kupon jika ingin mendapatkan keuntungan dari kupon tersebut. ')]</value>
      <webElementGuid>95671b0a-fad6-4215-9b17-08ea186f8d63</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
