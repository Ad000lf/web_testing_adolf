<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Unggah foto KTP untuk mengaktifkan akun OExpress dan mulai melakukan pengiriman barang</name>
   <tag></tag>
   <elementGuidId>fbbe8fa1-1b65-42e4-a31f-b41aaa176a9d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div/div/div/div/div/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.onboard-ktp.p-4 > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>b0ea36f4-125d-45cc-bcac-562c7b32790a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Unggah foto KTP untuk mengaktifkan akun OExpress dan mulai melakukan pengiriman barang.</value>
      <webElementGuid>1da3a211-159f-4282-bc18-b96bbb5c0ebe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-ktp p-4&quot;]/p[1]</value>
      <webElementGuid>3a393af8-6f8f-423e-850a-688cda1748a6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/p</value>
      <webElementGuid>ffb6c0fb-d328-43b6-874a-9fba92ed719b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Diri'])[1]/following::p[1]</value>
      <webElementGuid>506fa0b2-6a8c-4d6d-bdb4-3bea06f1aa6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/following::p[1]</value>
      <webElementGuid>39bdb546-55fa-47bd-8aca-3dfa61c2272a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih atau tarik file ke sini'])[1]/preceding::p[1]</value>
      <webElementGuid>ace673ca-d1e7-49a5-865d-b382ee9189bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ukuran file maksimal 2MB'])[1]/preceding::p[1]</value>
      <webElementGuid>7f03f587-3ae6-4d67-aa34-62d413263215</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Unggah foto KTP untuk mengaktifkan akun OExpress dan mulai melakukan pengiriman barang.']/parent::*</value>
      <webElementGuid>94fa2004-1a23-4d44-8cd2-7b9b15ca92f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/p</value>
      <webElementGuid>7951f0c5-8c93-445e-aced-ea770863ba93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Unggah foto KTP untuk mengaktifkan akun OExpress dan mulai melakukan pengiriman barang.' or . = 'Unggah foto KTP untuk mengaktifkan akun OExpress dan mulai melakukan pengiriman barang.')]</value>
      <webElementGuid>7eea20c5-c67f-4dc1-8106-a26c0e9dafe0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
