<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>panel_Upload_KTP</name>
   <tag></tag>
   <elementGuidId>a771a652-a02f-49b6-8f60-0b73f5885fee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div[2]/div/div/div/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.dnd-uploader</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>aecdbcf1-f64d-4349-87e8-1a609b616f7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dnd-uploader</value>
      <webElementGuid>ff37ee75-d7f8-426f-802c-8767582d4118</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih atau tarik file ke siniUkuran file maksimal 2MB</value>
      <webElementGuid>3f30845a-a937-4b1d-97a4-0d5a5d6b2657</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-ktp p-4&quot;]/div[1]/div[@class=&quot;dnd-uploader&quot;]</value>
      <webElementGuid>0b0d6db0-6577-4ab3-8ffc-bfed2eaad904</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/div/div</value>
      <webElementGuid>e9319310-382f-42f8-afe6-b48793658011</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Diri'])[1]/following::div[2]</value>
      <webElementGuid>54abb708-2fb9-41d5-8aa8-0f4e7b5d6fb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/following::div[9]</value>
      <webElementGuid>115f6d67-40cd-4cd4-9605-4210ba4ef180</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div</value>
      <webElementGuid>9a2cc8a1-d7d3-4c6d-ad90-6502dc4123bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Pilih atau tarik file ke siniUkuran file maksimal 2MB' or . = 'Pilih atau tarik file ke siniUkuran file maksimal 2MB')]</value>
      <webElementGuid>2b3f6ec6-a6f6-4a3d-8487-98c0bce831fe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
