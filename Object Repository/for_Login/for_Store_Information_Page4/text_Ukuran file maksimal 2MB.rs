<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Ukuran file maksimal 2MB</name>
   <tag></tag>
   <elementGuidId>49c07a1d-4c5e-46cc-8966-415601d5aaef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div[2]/div/div/div/div/div/div/div/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.mt-1.fw-bold</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4d3953ed-1ae4-421a-8357-7ecdd8543b1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mt-1 fw-bold</value>
      <webElementGuid>5766edcf-4480-4eb5-9e8f-b8253f0d5c79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ukuran file maksimal 2MB</value>
      <webElementGuid>3a4fab7b-2a38-462c-9cf1-bce09fee9c6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-lg modal-onboarding&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[@class=&quot;modal-onboarding--content&quot;]/div[@class=&quot;onboard-ktp p-4&quot;]/div[1]/div[@class=&quot;dnd-uploader&quot;]/div[1]/div[@class=&quot;mt-1 fw-bold&quot;]</value>
      <webElementGuid>08724093-24cf-4571-9908-c46b9c21f634</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div[2]/div/div/div/div/div/div/div/div/div[2]</value>
      <webElementGuid>2eda50ac-ed0a-46b5-8dcb-c8e61be604b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih atau tarik file ke sini'])[1]/following::div[1]</value>
      <webElementGuid>6dbc7fe0-f84a-4411-81a4-1d5705512fa2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Diri'])[1]/following::div[5]</value>
      <webElementGuid>9284def0-cb0b-4ef4-a9bd-f5a20f8b9be4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kode Kupon'])[1]/preceding::div[1]</value>
      <webElementGuid>848ee8b0-a9d5-43a6-8d2c-fe74abfcfb9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ukuran file maksimal 2MB']/parent::*</value>
      <webElementGuid>069989d4-2641-4681-9b1d-a455d8ae0658</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div/div/div/div[2]</value>
      <webElementGuid>8eab2406-e1a9-43ab-8b5b-9b5ff3af0489</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Ukuran file maksimal 2MB' or . = 'Ukuran file maksimal 2MB')]</value>
      <webElementGuid>e886e9fe-c5eb-4732-adb4-28e6496b190d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
