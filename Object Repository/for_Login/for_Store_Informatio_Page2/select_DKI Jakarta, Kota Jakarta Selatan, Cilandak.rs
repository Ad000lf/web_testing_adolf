<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_DKI Jakarta, Kota Jakarta Selatan, Cilandak</name>
   <tag></tag>
   <elementGuidId>41af34e9-5957-4efa-9d82-b8845e8be2cd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;null-0&quot;]/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>99ea0b1c-83a2-4c4c-8855-874aeb32de05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>DKI Jakarta, Kota Jakarta Selatan, Cilandak</value>
      <webElementGuid>2bb2888f-fb4d-4d36-994e-3924c065bd7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;null-0&quot;)/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>6092bd0f-c72c-4bdd-aacb-94a6c630057c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-0']/span/span</value>
      <webElementGuid>e3555a4d-e719-4b4c-97d6-9fb7360f74cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lokasi Kota / Kecamatan'])[1]/following::span[2]</value>
      <webElementGuid>566fa6cc-c00f-4df3-afd0-b26c89e62a84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alamat tidak boleh kosong.'])[1]/following::span[2]</value>
      <webElementGuid>7cad1705-888b-479b-a487-7314b3721d22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='DKI Jakarta, Kota Jakarta Selatan, Cilandak']/parent::*</value>
      <webElementGuid>e894d25c-d773-4546-8dc7-8ad8ade22df3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/span/span</value>
      <webElementGuid>42662f6b-1372-4568-8b99-7ac907e0728b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'DKI Jakarta, Kota Jakarta Selatan, Cilandak' or . = 'DKI Jakarta, Kota Jakarta Selatan, Cilandak')]</value>
      <webElementGuid>2bb11381-bbd4-488a-96d0-0a0efcac95a1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
