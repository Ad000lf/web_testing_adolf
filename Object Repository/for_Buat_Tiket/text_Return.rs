<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Return</name>
   <tag></tag>
   <elementGuidId>bb1000f3-5d80-4d14-8546-39d5ce97bb1a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option--selected.multiselect__option > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='select-layout-0']/span/span)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>bbb30670-54c6-4cda-b0f0-9b41f0749cb8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Return</value>
      <webElementGuid>fe8fb1f5-015e-45b0-9a02-f696b890298e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[3]/div[@class=&quot;p-monitoring&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[1]/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-md modal-add-bank&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[1]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;multiselect--active multiselect&quot;]/div[@class=&quot;multiselect__content-wrapper&quot;]/ul[@id=&quot;listbox-select-layout&quot;]/li[@id=&quot;select-layout-0&quot;]/span[@class=&quot;multiselect__option--highlight multiselect__option--selected multiselect__option&quot;]/span[1]</value>
      <webElementGuid>3ddad304-6999-4177-98b4-c60545752869</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='select-layout-0']/span/span)[2]</value>
      <webElementGuid>4da034ad-fe69-4ede-b132-0e8b3d3d607a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Aksi'])[1]/following::span[3]</value>
      <webElementGuid>086622e4-5d41-4d95-88d5-c68d7fc5bcf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reschedule'])[1]/preceding::span[1]</value>
      <webElementGuid>de93849f-25f7-4184-b744-77935fad895c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Update Data'])[1]/preceding::span[3]</value>
      <webElementGuid>58ad772f-781c-4b55-9e60-9a21a77eb13f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[3]/ul/li/span/span</value>
      <webElementGuid>e7bccbee-f392-444a-af6f-63d06b52e36c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Return' or . = 'Return')]</value>
      <webElementGuid>67c71486-6630-4308-9d95-6155a95ca69f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
