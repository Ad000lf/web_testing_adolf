<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Update Data</name>
   <tag></tag>
   <elementGuidId>b2894abf-c17c-444b-a4d7-86fc4581819f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#select-layout-2 > span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='select-layout-2']/span/span)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>426c60e7-888f-4879-9131-4b0db0d9fc91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Update Data</value>
      <webElementGuid>4f5f9562-e316-4f1c-9932-b5b26ed9180e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[3]/div[@class=&quot;p-monitoring&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[1]/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-md modal-add-bank&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[1]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;multiselect--active multiselect&quot;]/div[@class=&quot;multiselect__content-wrapper&quot;]/ul[@id=&quot;listbox-select-layout&quot;]/li[@id=&quot;select-layout-2&quot;]/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>cf72cba9-c16f-4aa2-8f7e-855b23332523</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='select-layout-2']/span/span)[2]</value>
      <webElementGuid>b097ea02-5c50-4a24-b4bd-bb0114041538</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reschedule'])[1]/following::span[2]</value>
      <webElementGuid>99a63e17-f79a-482e-af2a-e19e007cd012</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Return'])[1]/following::span[4]</value>
      <webElementGuid>1c703456-9e6f-4abb-8a15-102e4c28f779</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ajukan Claim'])[1]/preceding::span[1]</value>
      <webElementGuid>b2c00fdb-854f-4b8f-88ae-c74bb7a5f01f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Update Data']/parent::*</value>
      <webElementGuid>c9e5bfd9-5fa3-48ce-9c3a-5cd0a422511e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[3]/ul/li[3]/span/span</value>
      <webElementGuid>0d91c8f4-c971-4cb3-810c-cede452e7060</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Update Data' or . = 'Update Data')]</value>
      <webElementGuid>244215f5-51b0-4413-b6d1-6097de8c984f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
