<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Ajukan Claim</name>
   <tag></tag>
   <elementGuidId>3883c224-e1de-4286-a4ad-47ff21da7cd4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#select-layout-3 > span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(//li[@id='select-layout-3']/span/span)[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7649e5f8-7e62-42f9-949d-ba568c1ab558</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ajukan Claim</value>
      <webElementGuid>f5dcd367-d8a4-41de-b875-15416990054e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[3]/div[@class=&quot;p-monitoring&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[1]/div[@class=&quot;modal fade show d-block&quot;]/div[@class=&quot;modal-dialog modal-dialog-centered modal-md modal-add-bank&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body pb-3&quot;]/div[1]/div[@class=&quot;mb-3&quot;]/div[@class=&quot;multiselect--active multiselect&quot;]/div[@class=&quot;multiselect__content-wrapper&quot;]/ul[@id=&quot;listbox-select-layout&quot;]/li[@id=&quot;select-layout-3&quot;]/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>a91af06b-b4ca-48da-aaa7-45c8ab1b7200</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>(//li[@id='select-layout-3']/span/span)[2]</value>
      <webElementGuid>edf957cf-04d6-434a-9cd7-32a9b180145f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Update Data'])[1]/following::span[2]</value>
      <webElementGuid>5906fddd-4df3-4bf9-9cb4-2fcf2b30aeeb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Reschedule'])[1]/following::span[4]</value>
      <webElementGuid>94d3ef21-8436-40ae-8a2f-4e66b8c0db95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ajukan Claim']/parent::*</value>
      <webElementGuid>55a2b42b-4127-413d-9c53-0115dc0e583a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[3]/ul/li[4]/span/span</value>
      <webElementGuid>b5961434-0684-40d9-9302-398a8f0cce12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Ajukan Claim' or . = 'Ajukan Claim')]</value>
      <webElementGuid>87dd8e19-b296-4f27-ab7e-26562f57b47a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
