<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_Drop Sendiri</name>
   <tag></tag>
   <elementGuidId>a834f7de-9e47-4f36-a646-31e168298ab1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[2]/div/div/div/div/div/div[2]/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>911af565-ffc5-420d-ab2c-ea8ed85fb8bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-center me-1</value>
      <webElementGuid>8c4ee573-414f-4e4b-b8a0-09ddbd333591</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Drop Sendiri</value>
      <webElementGuid>43471be2-891f-477c-8f44-462f35bc5da1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[3]/div[@class=&quot;p-create-shipment content&quot;]/div[@class=&quot;container mt-3&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;form-data-pickup&quot;]/div[@class=&quot;py-3&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;select-box&quot;]/div[@class=&quot;select-box--item justify-content-center&quot;]/div[@class=&quot;text-center me-1&quot;]</value>
      <webElementGuid>2469220e-e1d4-4b86-8f76-7693d3065f4e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div/div[2]/div/div/div/div/div/div[2]/div[2]/div</value>
      <webElementGuid>a882d5a2-2251-420d-9840-bf9f9816f096</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Terjadwal'])[1]/following::div[2]</value>
      <webElementGuid>50e1d1f8-a471-4232-a873-e7eca8b8bb50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jenis Pengiriman'])[1]/following::div[5]</value>
      <webElementGuid>981163cf-a446-40b6-854c-ecb7768b49e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tanggal Penjemputan'])[1]/preceding::div[1]</value>
      <webElementGuid>3c977057-7e55-4493-aedc-f8dfec2c3518</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jam Penjemputan'])[1]/preceding::div[6]</value>
      <webElementGuid>cf19cdec-39b3-4388-bee3-66c76246afa2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Drop Sendiri']/parent::*</value>
      <webElementGuid>d2c56fc7-95ee-47f3-84e1-627181c742bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/div/div[2]/div[2]/div</value>
      <webElementGuid>b4dd6350-9e42-4805-9592-ee0a12501e0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Drop Sendiri' or . = 'Drop Sendiri')]</value>
      <webElementGuid>8e9c8999-65c0-49ce-94ef-a5696d12f0f2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
