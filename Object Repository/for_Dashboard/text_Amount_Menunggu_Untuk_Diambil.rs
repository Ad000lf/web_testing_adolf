<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Amount_Menunggu_Untuk_Diambil</name>
   <tag></tag>
   <elementGuidId>bb6d76bd-f75a-4bec-8982-9d8d910623b2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div/div[5]/div/div/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h3.my-2.card-overview__value.text-info</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>1e4ad372-3056-4623-aaac-d9cc2ef3464f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>my-2 card-overview__value text-info</value>
      <webElementGuid>ecc6a0aa-eada-4d1a-a94f-3e54d8449393</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>f16a83ad-6de7-4901-b5ee-3feaebbac22a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;home&quot;]/div[@class=&quot;p-dashboard&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-4&quot;]/div[@class=&quot;card widget-flat card-overview text-center null&quot;]/div[@class=&quot;card-body null&quot;]/h3[@class=&quot;my-2 card-overview__value text-info&quot;]</value>
      <webElementGuid>ac620df2-0f74-4054-ae14-4fdb76153eb6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div/div[5]/div/div/div/h3</value>
      <webElementGuid>83dfb752-d696-48cc-8189-20a0947df861</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ringkasan Kiriman'])[1]/following::h3[1]</value>
      <webElementGuid>c6f19b99-0d39-4710-be16-c7aef170c8e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Selengkapnya'])[3]/following::h3[1]</value>
      <webElementGuid>89d53124-adac-45c0-888d-a241bd7f9590</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menunggu Untuk Diambil'])[1]/preceding::h3[1]</value>
      <webElementGuid>37f2ec9d-550a-4d78-9614-8a79288b5186</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Proses Pengiriman'])[1]/preceding::h3[2]</value>
      <webElementGuid>6ac9c0c6-b4db-48c3-b485-7c505ced70c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/h3</value>
      <webElementGuid>62032695-83f5-4e7b-a28a-02acc8d65376</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = '0' or . = '0')]</value>
      <webElementGuid>a32c06f4-08af-4d77-9036-69ae65eac6bd</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
