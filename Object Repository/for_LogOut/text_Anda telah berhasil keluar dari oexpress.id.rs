<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Anda telah berhasil keluar dari oexpress.id</name>
   <tag></tag>
   <elementGuidId>81f2b20c-afba-4d8d-83db-54dbe3d1d323</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p.text-muted.mb-4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>8abf3d28-4041-4494-92df-2f0423f4e981</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-muted mb-4</value>
      <webElementGuid>fe82c74e-2d97-4e2e-85f0-9986a7e9442f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Anda telah berhasil keluar dari oexpress.id</value>
      <webElementGuid>0f702ea1-7cf9-472c-8d0f-f668971199b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;layout-auth&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;text-center&quot;]/p[@class=&quot;text-muted mb-4&quot;]</value>
      <webElementGuid>358bf8eb-63e4-4dba-9c11-d94c5426a9f8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/p</value>
      <webElementGuid>6d6dcff1-ab9a-4ba3-a55f-5403807145fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sampai Jumpa Kembali!'])[1]/following::p[1]</value>
      <webElementGuid>b7f599c9-a7cc-4bfd-aed1-9d0c65779a42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log In'])[1]/preceding::p[1]</value>
      <webElementGuid>ad32921c-ce1e-42a0-a4d3-f83c8999c31a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::p[2]</value>
      <webElementGuid>470256e5-235d-4797-aa1c-4203d9557c22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Anda telah berhasil keluar dari oexpress.id']/parent::*</value>
      <webElementGuid>82860ed8-a1aa-4b2a-8631-bba5f8476b6a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p</value>
      <webElementGuid>7145afb1-1510-4465-b0c1-f7b467728a18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Anda telah berhasil keluar dari oexpress.id' or . = 'Anda telah berhasil keluar dari oexpress.id')]</value>
      <webElementGuid>a1bb3384-17ba-4fbe-8411-b26719ad192a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
