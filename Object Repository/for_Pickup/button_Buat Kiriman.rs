<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Buat Kiriman</name>
   <tag></tag>
   <elementGuidId>146a6d3f-a60c-4cc7-8f45-f052aae092c8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div[2]/div/div/div/div/table/tbody/tr/td/div/div/a/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-sm.btn-primary.mt-2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>41ecded8-cd9b-482e-ab50-e55d66768e41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-sm btn-primary mt-2</value>
      <webElementGuid>502739ce-1828-4890-8410-cbdc5e6f997e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Buat Kiriman </value>
      <webElementGuid>1661196e-5b22-4350-afe2-602ae0c25f6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-hover mb-0&quot;]/tbody[@class=&quot;table-hover&quot;]/tr[1]/td[1]/div[@class=&quot;d-flex align-items-center justify-content-center&quot;]/div[@class=&quot;text-center&quot;]/a[1]/button[@class=&quot;btn btn-sm btn-primary mt-2&quot;]</value>
      <webElementGuid>e889ecd3-fb4d-4fb8-a3cb-5320b1f3f049</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div[2]/div/div/div/div/table/tbody/tr/td/div/div/a/button</value>
      <webElementGuid>0e99990a-4844-42fb-8ccf-d17c4ad42160</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tidak Ada Kiriman'])[1]/following::button[1]</value>
      <webElementGuid>19be9458-26cd-498e-a3de-6dfbbc24aaa6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='#'])[1]/following::button[1]</value>
      <webElementGuid>1460ea98-571c-449a-a0f9-ac8359bb1227</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::button[1]</value>
      <webElementGuid>42ea422d-4f5a-42ec-98a6-c8144ecf7bd1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::button[1]</value>
      <webElementGuid>d8f47d53-5a7c-4d11-9fb7-420790f89843</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/button</value>
      <webElementGuid>1a09d939-15bf-4136-b0f6-ecc80add6902</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = ' Buat Kiriman ' or . = ' Buat Kiriman ')]</value>
      <webElementGuid>93061db4-d813-4fe2-bc4e-cc469a93f49c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
