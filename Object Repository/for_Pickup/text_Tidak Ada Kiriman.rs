<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Tidak Ada Kiriman</name>
   <tag></tag>
   <elementGuidId>76584848-300b-43f6-abbe-ece2f27dd08e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div[2]/div/div/div/div/table/tbody/tr/td/div/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.h4.mt-2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>361e9522-e6f3-44fc-9d8b-bfa0c7646e71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>h4 mt-2</value>
      <webElementGuid>4891db69-f7cd-41d7-850b-0309a71e98ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tidak Ada Kiriman</value>
      <webElementGuid>5a2b15a9-a0cd-48c7-82e9-bf887e2046cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[1]/div[@class=&quot;wrapper&quot;]/div[@class=&quot;content-page&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;table-responsive&quot;]/table[@class=&quot;table table-hover mb-0&quot;]/tbody[@class=&quot;table-hover&quot;]/tr[1]/td[1]/div[@class=&quot;d-flex align-items-center justify-content-center&quot;]/div[@class=&quot;text-center&quot;]/div[@class=&quot;h4 mt-2&quot;]</value>
      <webElementGuid>ad56831e-1c7a-45d0-bb91-e02498b49b5b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div[2]/div/div[3]/div/div[2]/div/div/div/div/table/tbody/tr/td/div/div/div[2]</value>
      <webElementGuid>d298d26b-07f2-4a4b-94d5-5af14a1414b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='#'])[1]/following::div[4]</value>
      <webElementGuid>9295aabc-3179-4438-9060-08df1e88fd3a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='% Terkirim'])[1]/following::div[4]</value>
      <webElementGuid>64034ea4-2ad2-4e2e-9783-41de497addb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Kiriman'])[2]/preceding::div[1]</value>
      <webElementGuid>3da0a34b-71be-48a6-8331-46838677c4b0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::div[3]</value>
      <webElementGuid>34f87874-f5cb-46d1-8ae9-c502691e023f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Tidak Ada Kiriman']/parent::*</value>
      <webElementGuid>16cf8903-7d81-4b7c-bafa-769fe58d82bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td/div/div/div[2]</value>
      <webElementGuid>280e58c7-ea1b-489e-94fb-4ebcb1905e14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Tidak Ada Kiriman' or . = 'Tidak Ada Kiriman')]</value>
      <webElementGuid>20d87bff-ca8c-4f50-9531-c4c3de7e1c7c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
