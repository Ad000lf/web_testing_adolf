<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Bulan ini</name>
   <tag></tag>
   <elementGuidId>6af84369-e142-43aa-b188-9812d4d9d24f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*/text()[normalize-space(.)='Bulan ini']/parent::*</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9fa7c7ee-7ffe-4174-b1ef-78ec8deb4bf2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dp__preset_range</value>
      <webElementGuid>a41bd245-71fe-4904-87de-00acce203e76</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bulan ini</value>
      <webElementGuid>b3ef2c4f-4d05-46f5-b942-4bbe2c43dfbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;dp__menu dp__menu_index dp__theme_light&quot;]/div[@class=&quot;dp__menu_content_wrapper&quot;]/div[@class=&quot;dp__preset_ranges&quot;]/div[@class=&quot;dp__preset_range&quot;]</value>
      <webElementGuid>66b14f88-ebb0-4719-a28c-70eda4a75fa8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bulan ini']/parent::*</value>
      <webElementGuid>9ba469c9-f7f5-432c-8aa7-d3ba59ea3816</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]</value>
      <webElementGuid>94db8ca7-e01f-4e48-a24f-d13e85971e45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Bulan ini' or . = 'Bulan ini')]</value>
      <webElementGuid>aaeb5b51-c879-4ecf-ace9-c10b1ca932ac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
