<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Bulan lalu</name>
   <tag></tag>
   <elementGuidId>1a572a3c-3a09-402b-b00b-070cd1824298</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[10]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>64fb51c8-a5d5-480d-9237-8463796f600b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dp__preset_range</value>
      <webElementGuid>00dbc845-933e-488a-8511-50581ca33f05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bulan lalu</value>
      <webElementGuid>04abcef3-8ed2-492b-9db9-0691444de696</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;dp__menu dp__menu_index dp__theme_light&quot;]/div[@class=&quot;dp__menu_content_wrapper&quot;]/div[@class=&quot;dp__preset_ranges&quot;]/div[@class=&quot;dp__preset_range&quot;]</value>
      <webElementGuid>013e44f1-3162-4fd4-82c5-dee25a351b4a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[10]</value>
      <webElementGuid>82153e2c-13f5-44a5-a683-0a5a31e84d75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bulan lalu']/parent::*</value>
      <webElementGuid>83e0ab90-2599-4f2a-99a2-ef8d6a7eeb5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[5]</value>
      <webElementGuid>cab31cf9-7553-4a0b-81af-c6d5826c305e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Bulan lalu' or . = 'Bulan lalu')]</value>
      <webElementGuid>d3c431ec-1bc0-4496-883d-8f97a52af28a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
