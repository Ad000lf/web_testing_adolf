<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>set_JT Express</name>
   <tag></tag>
   <elementGuidId>543caf2f-1146-459d-92c5-aa41adb2b2a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='null-3']/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5bd1c6ad-a478-4448-b5a9-9ab9a08554dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>J&amp;T Express</value>
      <webElementGuid>3b2e9e40-cbf0-4895-8e5f-a192c11abe5b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;null-3&quot;)/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>d2bb0c16-05c1-430f-ae3d-727873bf224e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-3']/span/span</value>
      <webElementGuid>381ca43d-aa76-4f38-8228-99c038960317</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='JNE Express'])[1]/following::span[2]</value>
      <webElementGuid>e6a58f3e-8e8d-4496-938a-0d422973c36f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SAP Express'])[1]/following::span[4]</value>
      <webElementGuid>df4d8d64-41fe-4734-8a6b-eb26d4208561</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ninja Xpress'])[1]/preceding::span[1]</value>
      <webElementGuid>13129d61-845b-45aa-80f4-7db58adf8aa0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ID Express'])[1]/preceding::span[3]</value>
      <webElementGuid>f446a772-a97d-4e53-a17a-2fa9b1fa4c57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='J&amp;T Express']/parent::*</value>
      <webElementGuid>2222b8ed-ec4f-47a3-bd1b-d64caad3268a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/span/span</value>
      <webElementGuid>3d378c2f-2181-4494-be71-2e82f801dca3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'J&amp;T Express' or . = 'J&amp;T Express')]</value>
      <webElementGuid>3eb2c667-dda6-4879-8d3f-f88da4df8e24</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
