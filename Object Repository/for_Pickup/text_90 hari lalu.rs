<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_90 hari lalu</name>
   <tag></tag>
   <elementGuidId>def66097-b5f9-4ad9-8d2c-58282fc91c12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[9]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8c30adeb-ca05-421a-b7a6-86bd8827d095</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dp__preset_range</value>
      <webElementGuid>696c8887-decc-4198-a939-75b196379cd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>90 hari lalu</value>
      <webElementGuid>b69fc880-f93b-4d33-836b-6be8c425341b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;dp__menu dp__menu_index dp__theme_light&quot;]/div[@class=&quot;dp__menu_content_wrapper&quot;]/div[@class=&quot;dp__preset_ranges&quot;]/div[@class=&quot;dp__preset_range&quot;]</value>
      <webElementGuid>5cb34833-af51-4eb2-be03-d6e0257b389b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[9]</value>
      <webElementGuid>a9f95dc4-605f-4c47-80d4-c6e9e847519a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='90 hari lalu']/parent::*</value>
      <webElementGuid>ba17f4fc-bc90-4f8c-a4d5-abdd8f16ca86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]/div/div[4]</value>
      <webElementGuid>42209c87-0e5d-49cc-9887-3605c7a04a76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '90 hari lalu' or . = '90 hari lalu')]</value>
      <webElementGuid>4da4f76c-f2e7-431d-9f54-58564d28bc1f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
