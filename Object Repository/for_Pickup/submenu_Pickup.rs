<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>submenu_Pickup</name>
   <tag></tag>
   <elementGuidId>05674fd1-0f5b-4397-96b9-fd87f0ca8bb2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[3]/div[2]/div/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.router-link-active.router-link-exact-active.side-nav-link.submenu > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>80d7e514-3a56-4ed3-b31b-6afef28e7a92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pickup</value>
      <webElementGuid>fdca20d9-af1e-43e6-b9e1-ebae2a1f3f5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;leftside-menu-container&quot;)/div[@class=&quot;simplebar-wrapper&quot;]/div[@class=&quot;simplebar-mask&quot;]/div[@class=&quot;simplebar-offset&quot;]/div[@class=&quot;simplebar-content-wrapper d-flex justify-content-between flex-column&quot;]/div[@class=&quot;simplebar-content&quot;]/div[@class=&quot;d-flex justify-content-between flex-column&quot;]/ul[@class=&quot;side-nav&quot;]/li[@class=&quot;side-nav-item&quot;]/div[@class=&quot;side-nav-submenu&quot;]/div[1]/a[@class=&quot;router-link-active router-link-exact-active side-nav-link submenu&quot;]/span[1]</value>
      <webElementGuid>cedb05d8-0185-49c9-a0f9-228a8880fff4</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='leftside-menu-container']/div/div[2]/div/div/div/div/ul/li[3]/div[2]/div/a/span</value>
      <webElementGuid>4ad4afb4-bedf-4190-bab3-25582240b544</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Kiriman'])[1]/following::span[1]</value>
      <webElementGuid>a8b614bf-71dc-4bcf-b838-8f5a2edf52ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::span[2]</value>
      <webElementGuid>aa20a5a3-a31d-4927-a7bc-86f018b49c5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manifest'])[1]/preceding::span[1]</value>
      <webElementGuid>4149b8eb-e372-40b2-bfec-892454768db8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Semua Kiriman'])[1]/preceding::span[2]</value>
      <webElementGuid>b43c3b4b-aca5-4b9c-a932-f906e4ba5f2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pickup']/parent::*</value>
      <webElementGuid>8a900ab9-f337-4bad-ae29-abca9d7c6192</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/a/span</value>
      <webElementGuid>9dcf7f0f-2a5f-4058-9a0c-a1af47d255b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Pickup' or . = 'Pickup')]</value>
      <webElementGuid>9c04f1fc-7e96-4dff-9855-181b3224dcce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
