<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_30 hari lalu</name>
   <tag></tag>
   <elementGuidId>f5f71787-67ac-482f-9833-9ddfdea5c6cc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[8]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2a5fe5b3-f34d-482a-a716-02577a2cc33b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dp__preset_range</value>
      <webElementGuid>67d80591-a8ca-420b-837f-faa324b0100a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>30 hari lalu</value>
      <webElementGuid>3eb6c7c4-157c-4fa9-affc-39b8a8a66774</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;dp__menu dp__menu_index dp__theme_light&quot;]/div[@class=&quot;dp__menu_content_wrapper&quot;]/div[@class=&quot;dp__preset_ranges&quot;]/div[@class=&quot;dp__preset_range&quot;]</value>
      <webElementGuid>1c8cf4ed-1685-479f-a815-adeb5946105b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[8]</value>
      <webElementGuid>cefb53b6-4315-4b13-ad23-7404c292282b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='30 hari lalu']/parent::*</value>
      <webElementGuid>e5048e1c-ac09-4fce-8ed0-1be9d568d482</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]/div/div[3]</value>
      <webElementGuid>6f48f390-4773-41a6-bf30-34a6bc7b1308</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '30 hari lalu' or . = '30 hari lalu')]</value>
      <webElementGuid>cac2ae29-f656-4dd4-9d0d-bd3b2a524d59</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
