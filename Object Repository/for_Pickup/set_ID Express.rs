<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>set_ID Express</name>
   <tag></tag>
   <elementGuidId>39e5248a-c24e-421f-bf93-c0cae431ed17</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='null-5']/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7d9472ac-6b76-4803-9a5e-a9ebb184a610</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ID Express</value>
      <webElementGuid>67ee8d44-1c35-4a33-bfc1-b55857453349</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;null-5&quot;)/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>9f83cd95-ad01-4f1e-aa31-b3b80963ebaa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-5']/span/span</value>
      <webElementGuid>b055c1f2-9587-418a-8a17-366cceec6d36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ninja Xpress'])[1]/following::span[2]</value>
      <webElementGuid>18206b27-d96f-4c90-8e8e-a48e746ab1dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='J&amp;T Express'])[1]/following::span[4]</value>
      <webElementGuid>95c9ddbe-8626-4296-b4d2-49136f3bbf87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Paxel'])[1]/preceding::span[1]</value>
      <webElementGuid>13a046c5-6224-4a67-97ca-bf555f545de4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ID Express']/parent::*</value>
      <webElementGuid>8fa35a4c-9ca0-4ba5-b5c3-7a9b607b2500</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/span/span</value>
      <webElementGuid>0262f57c-b577-4b72-9ffe-9ce10ab31b64</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'ID Express' or . = 'ID Express')]</value>
      <webElementGuid>85c5a6cf-0bfe-4ab5-b6ae-b832afadccb8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
