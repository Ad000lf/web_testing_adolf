<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>set_Ninja Xpress</name>
   <tag></tag>
   <elementGuidId>1d95f0d8-4efe-4d9d-a411-e724613eb18b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='null-4']/span/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.multiselect__option--highlight.multiselect__option > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>72fd7b57-7b26-43ea-a55c-8cdef8252637</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ninja Xpress</value>
      <webElementGuid>e17e4c00-1e05-4956-80d9-e940eb7220da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;null-4&quot;)/span[@class=&quot;multiselect__option--highlight multiselect__option&quot;]/span[1]</value>
      <webElementGuid>ad8a0871-443a-4312-ae3b-160c5c75ff89</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='null-4']/span/span</value>
      <webElementGuid>38b17a6d-665f-461b-b33f-fda6deef248f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='J&amp;T Express'])[1]/following::span[2]</value>
      <webElementGuid>9c430a63-c096-4b7c-a9af-d8bf9b4536d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='JNE Express'])[1]/following::span[4]</value>
      <webElementGuid>fcf21c53-dbe8-40be-a2ea-f8169077ec84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ID Express'])[1]/preceding::span[1]</value>
      <webElementGuid>9dbde680-5821-4756-a945-99a4eb3163fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Paxel'])[1]/preceding::span[3]</value>
      <webElementGuid>8c226f8b-e0e7-4623-8a49-7b92573dcd60</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ninja Xpress']/parent::*</value>
      <webElementGuid>c6c6db50-068e-408b-8bdc-fb19dabb2828</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/span/span</value>
      <webElementGuid>dea984fe-43c5-4557-b3a6-7677e8e2b900</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Ninja Xpress' or . = 'Ninja Xpress')]</value>
      <webElementGuid>95946c4e-b998-47f7-b873-f0103604d3af</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
