<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_7 hari lalu</name>
   <tag></tag>
   <elementGuidId>fc9de554-2fef-443a-9cfc-3c5167b10db0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[7]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>273cc91b-651d-4b0f-a33e-4117de664401</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dp__preset_range</value>
      <webElementGuid>4ada3fc2-4828-4716-9d13-df7be250be7e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>7 hari lalu</value>
      <webElementGuid>ac386367-f300-446b-ad92-f0378bb59ac6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;dp__menu dp__menu_index dp__theme_light&quot;]/div[@class=&quot;dp__menu_content_wrapper&quot;]/div[@class=&quot;dp__preset_ranges&quot;]/div[@class=&quot;dp__preset_range&quot;]</value>
      <webElementGuid>f70dd6b1-e479-40c5-8fd0-d41ddac85de1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::div[7]</value>
      <webElementGuid>45caef16-3d92-4669-a2b0-258280d4715a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='7 hari lalu']/parent::*</value>
      <webElementGuid>9e683c20-2988-4c53-ac75-2270337fda02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]/div/div[2]</value>
      <webElementGuid>3cddf6dc-69cf-4275-ac9a-82ca1cca1c22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '7 hari lalu' or . = '7 hari lalu')]</value>
      <webElementGuid>55f92533-4d3c-46ed-b93a-bbddc499c4a4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
